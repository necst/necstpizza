$(function() {
	$('[name="pizzatype"]').each(
		function(index) {
			$(this).on('click', function() {
				showPizzaField($(this).val());
			});
		});
	showPizzaField($('[name="pizzatype"]:checked').val())
});

var radioCurrentChoice = -1

function showPizzaField(value) {
	if (value != radioCurrentChoice) {
		if (value=="0") {
			$('.base-field').hide();	
			$('.price-field').show();
		} else if (value=="1") {
			$('.base-field').show();
			$('.price-field').hide();
		} else {
			$('.base-field').hide();
			$('.price-field').hide();
		}
		radioCurrentChoice = value
	}
}