$(function() {
	$('.selectpicker').selectpicker();
	setRemoveIngredientsForPizza();
});

function setRemoveIngredientsForPizza() {
	val = $("[id='chosenpizza'] option:selected").val()
	$.ajax({
		url: "/pizza/ingredients/"+val,
		dataType: "json",
		success: function(data) {
			var $el = $("[id='removeingredients']")
			$el.empty()
			$.each(data, function(id) {
				$el.append($("<option></option>")
					.attr("value", data[id].Id).text(data[id].Name));
			});
			$el.selectpicker('refresh');
		}
	});
}

$(function () {
	$("[id='chosenpizza']").change( function(){
		setRemoveIngredientsForPizza()
	})
});