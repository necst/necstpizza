$(function() {
	$('#calendar').fullCalendar({
		events: {
	        url: '/event/list/json',
	        color: 'green',
	    },
	    eventClick: function(calEvent, jsEvent, view) {
	    	location.href=calEvent.url;
	    }
    })
})