// +build gotask

package tasks

import (
	"necstpizza/app/models"
	"strconv"
	// "database/sql"
	"fmt"
	"github.com/coopernurse/gorp"
	"github.com/jingweno/gotask/tasking"
	_ "github.com/mattn/go-sqlite3"
	r "github.com/revel/revel"
	"github.com/revel/revel/modules/db/app"
	"reflect"
)

// NAME
//    create-tables - Create tables on db if they do not exist
//
// DESCRIPTION
//    Create db tables for GoPizza app
//
// OPTIONS
//
func TaskCreateTables(task *tasking.T) {
	db.Init()
	Dbm := &gorp.DbMap{Db: db.Db, Dialect: gorp.SqliteDialect{}}

	createTableFromModel(Dbm, models.Ingredient{})
	createTableFromModel(Dbm, models.Pizza{})
	createTableFromModel(Dbm, models.PizzaToIngredient{})
	// createTableFromModel(Dbm, models.Role{})

	Dbm.TraceOn("[gorp]", r.INFO)
	Dbm.DropTables()
	Dbm.CreateTables()

	ingrs := []*models.Ingredient{
		&models.Ingredient{0, "Mozzarella"},
		&models.Ingredient{0, "Salame piccante"},
		&models.Ingredient{0, "Patatine fritte"},
	}

	for _, ingr := range ingrs {
		if err := Dbm.Insert(ingr); err != nil {
			panic(err)
		}
		fmt.Printf("Inserted ingredient: [%d] %s\n", ingr.IngredientId, ingr.Name)
	}

	pizzas := []*models.Pizza{
		&models.Pizza{0, "Margherita", 5.0, []models.Ingredient{*ingrs[0]}},
		&models.Pizza{0, "Diavola", 5.0, []models.Ingredient{*ingrs[0], *ingrs[1]}},
		&models.Pizza{0, "Patatine", 5.0, []models.Ingredient{*ingrs[0], *ingrs[2]}},
	}
	for _, pizza := range pizzas {
		if err := Dbm.Insert(pizza); err != nil {
			panic(err)
		}
	}

	// roles := []*models.Role{
	// 	&models.Role{"alessandro.frossi", 0},
	// }
	// for _, role := range roles {
	// 	if err := Dbm.Insert(role); err != nil {
	// 		panic(err)
	// 	}
	// }
}

func createTableFromModel(Dbm *gorp.DbMap, m interface{}) {
	// setColumnSizes := func(t *gorp.TableMap, colSizes map[string]int) {
	// 	for col, size := range colSizes {
	// 		t.ColMap(col).MaxSize = size
	// 	}
	// }

	// Keys
	mType := reflect.TypeOf(m)
	keys := make([]string, 0)
	autoInc := false
	for i := 0; i < mType.NumField(); i++ {
		if mType.Field(i).Tag.Get("db_autoincrement") != "" {
			autoInc = true
		}
		if mType.Field(i).Tag.Get("db_key") != "" {
			keys = append(keys, mType.Field(i).Name)
		}
	}

	// Create table
	t := Dbm.AddTable(m).SetKeys(autoInc, keys...)
	for i := 0; i < mType.NumField(); i++ {
		if mType.Field(i).Tag.Get("db_transient") != "" {
			t.ColMap(mType.Field(i).Name).Transient = true
		}
		if sz := mType.Field(i).Tag.Get("db_size"); sz != "" {
			szI, err := strconv.ParseInt(sz, 10, 64)
			if err != nil {
				panic(err)
			}
			t.ColMap(mType.Field(i).Name).MaxSize = int(szI)
		}
	}
}
