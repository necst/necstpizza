package jobs

import (
	"github.com/revel/modules/jobs/app/jobs"
	"github.com/revel/revel"
	"html/template"
	"necstpizza/app/controllers"
	"necstpizza/app/models"
	"time"
)

// type OrderItem struct {
// 	user string
// 	pizzas []*controller.PizzaEntry
// }

type OrderSummary struct{}

func (c OrderSummary) Run() {
	// get all events still on time but past notification time
	var query string
	dbDriver := revel.Config.StringDefault("db.driver", "sqlite3")
	if dbDriver == "sqlite3" {
		query = "select * from Event where datetime('now', 'localtime') > NotificationTimeString and Notified = 0"
	} else if dbDriver == "mymysql" {
		query = "select * from Event where NOW() > NotificationTimeString and Notified = 0"
	}

	events, err := controllers.Dbm.Select(models.Event{}, query)

	if err != nil {
		panic(err)
	}

	for _, tmp := range events {
		ev := tmp.(*models.Event)
		if ev.Active {
			revel.INFO.Println("Creating and sending summary for event:", ev.Name)
			// get orders for event
			bevList := make(map[string]int)
			pizzaList := make(map[string]*controllers.PizzaEntry)
			totals := make(map[string]float32)
			orderList := make(map[string]map[string]*controllers.PizzaEntry)
			orders, err := controllers.Dbm.Select(models.PizzaOrder{},
				"select * from PizzaOrder where EventId = ?", ev.EventId)
			var total float32 = 0.0

			for _, ord := range orders {
				if !ord.(*models.PizzaOrder).Ignore {
					if _, ok := bevList[ord.(*models.PizzaOrder).Drink.Name]; !ok {
						bevList[ord.(*models.PizzaOrder).Drink.Name] = 0
					}
					bevList[ord.(*models.PizzaOrder).Drink.Name] += ord.(*models.PizzaOrder).Qty

					hash := ord.(*models.PizzaOrder).Hash()
					if _, ok := pizzaList[hash]; !ok {
						pizzaList[hash] = &controllers.PizzaEntry{
							template.HTML(ord.(*models.PizzaOrder).Pizza.SpecialName()),
							ord.(*models.PizzaOrder).AdditionList(),
							ord.(*models.PizzaOrder).RemovalList(),
							0,
						}
					}
					if _, ok := orderList[ord.(*models.PizzaOrder).UserId]; !ok {
						orderList[ord.(*models.PizzaOrder).UserId] = make(map[string]*controllers.PizzaEntry)
					}
					if _, ok := orderList[ord.(*models.PizzaOrder).UserId][hash]; !ok {
						orderList[ord.(*models.PizzaOrder).UserId][hash] = &controllers.PizzaEntry{
							template.HTML(ord.(*models.PizzaOrder).Pizza.SpecialName()),
							ord.(*models.PizzaOrder).AdditionList(),
							ord.(*models.PizzaOrder).RemovalList(),
							0,
						}
					}
					pizzaList[hash].Qty += ord.(*models.PizzaOrder).Qty
					orderList[ord.(*models.PizzaOrder).UserId][hash].Qty += ord.(*models.PizzaOrder).Qty

					if !ord.(*models.PizzaOrder).Paid {
						if _, ok := totals[ord.(*models.PizzaOrder).UserId]; !ok {
							totals[ord.(*models.PizzaOrder).UserId] = 0.0
						}
						totals[ord.(*models.PizzaOrder).UserId] += ord.(*models.PizzaOrder).Price(ev.SpecialPrice)
						total += ord.(*models.PizzaOrder).Price(ev.SpecialPrice)
					}
				}
			}

			//get user
			u, err := controllers.Dbm.Get(models.User{}, ev.ManagerId)
			if err != nil {
				panic(err)
			}

			err = controllers.SendSummary(ev, u.(*models.User), pizzaList, bevList, totals, total, orderList)
			if err != nil {
				panic(err)
			}

			ev.Notified = true
			_, err = controllers.Dbm.Update(ev)
			if err != nil {
				panic(err)
			}
		}
	}
}

func init() {
	revel.OnAppStart(func() {
		jobs.Every(5*time.Minute, OrderSummary{})
	})
}
