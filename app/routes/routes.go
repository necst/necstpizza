// GENERATED CODE - DO NOT EDIT
package routes

import "github.com/revel/revel"


type tGorpController struct {}
var GorpController tGorpController


func (_ tGorpController) Begin(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("GorpController.Begin", args).Url
}

func (_ tGorpController) Commit(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("GorpController.Commit", args).Url
}

func (_ tGorpController) Rollback(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("GorpController.Rollback", args).Url
}


type tStatic struct {}
var Static tStatic


func (_ tStatic) Serve(
		prefix string,
		filepath string,
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "prefix", prefix)
	revel.Unbind(args, "filepath", filepath)
	return revel.MainRouter.Reverse("Static.Serve", args).Url
}

func (_ tStatic) ServeModule(
		moduleName string,
		prefix string,
		filepath string,
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "moduleName", moduleName)
	revel.Unbind(args, "prefix", prefix)
	revel.Unbind(args, "filepath", filepath)
	return revel.MainRouter.Reverse("Static.ServeModule", args).Url
}


type tTestRunner struct {}
var TestRunner tTestRunner


func (_ tTestRunner) Index(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("TestRunner.Index", args).Url
}

func (_ tTestRunner) Run(
		suite string,
		test string,
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "suite", suite)
	revel.Unbind(args, "test", test)
	return revel.MainRouter.Reverse("TestRunner.Run", args).Url
}

func (_ tTestRunner) List(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("TestRunner.List", args).Url
}


type tJobs struct {}
var Jobs tJobs


func (_ tJobs) Status(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("Jobs.Status", args).Url
}


type tAdmin struct {}
var Admin tAdmin


func (_ tAdmin) Index(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("Admin.Index", args).Url
}

func (_ tAdmin) ListElements(
		name string,
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "name", name)
	return revel.MainRouter.Reverse("Admin.ListElements", args).Url
}

func (_ tAdmin) AddElement(
		name string,
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "name", name)
	return revel.MainRouter.Reverse("Admin.AddElement", args).Url
}


type tApp struct {}
var App tApp


func (_ tApp) AddUser(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("App.AddUser", args).Url
}

func (_ tApp) AddEvents(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("App.AddEvents", args).Url
}

func (_ tApp) Index(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("App.Index", args).Url
}

func (_ tApp) Login(
		username string,
		password string,
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "username", username)
	revel.Unbind(args, "password", password)
	return revel.MainRouter.Reverse("App.Login", args).Url
}

func (_ tApp) Logout(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("App.Logout", args).Url
}


type tEvents struct {}
var Events tEvents


func (_ tEvents) CalendarView(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("Events.CalendarView", args).Url
}

func (_ tEvents) EventsToJson(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("Events.EventsToJson", args).Url
}

func (_ tEvents) ListEvents(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("Events.ListEvents", args).Url
}

func (_ tEvents) ListOldEvents(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("Events.ListOldEvents", args).Url
}

func (_ tEvents) ListRecurrentEvents(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("Events.ListRecurrentEvents", args).Url
}

func (_ tEvents) CreateEventView(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("Events.CreateEventView", args).Url
}

func (_ tEvents) EditEventView(
		id int,
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "id", id)
	return revel.MainRouter.Reverse("Events.EditEventView", args).Url
}

func (_ tEvents) EditEvent(
		id int,
		name string,
		description string,
		addInfo string,
		when string,
		notification string,
		repeat string,
		manager string,
		special string,
		notify string,
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "id", id)
	revel.Unbind(args, "name", name)
	revel.Unbind(args, "description", description)
	revel.Unbind(args, "addInfo", addInfo)
	revel.Unbind(args, "when", when)
	revel.Unbind(args, "notification", notification)
	revel.Unbind(args, "repeat", repeat)
	revel.Unbind(args, "manager", manager)
	revel.Unbind(args, "special", special)
	revel.Unbind(args, "notify", notify)
	return revel.MainRouter.Reverse("Events.EditEvent", args).Url
}

func (_ tEvents) CreateEvent(
		name string,
		description string,
		addInfo string,
		when string,
		notification string,
		repeat string,
		manager string,
		special string,
		notify string,
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "name", name)
	revel.Unbind(args, "description", description)
	revel.Unbind(args, "addInfo", addInfo)
	revel.Unbind(args, "when", when)
	revel.Unbind(args, "notification", notification)
	revel.Unbind(args, "repeat", repeat)
	revel.Unbind(args, "manager", manager)
	revel.Unbind(args, "special", special)
	revel.Unbind(args, "notify", notify)
	return revel.MainRouter.Reverse("Events.CreateEvent", args).Url
}

func (_ tEvents) DeleteEvent(
		id int,
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "id", id)
	return revel.MainRouter.Reverse("Events.DeleteEvent", args).Url
}


type tOrders struct {}
var Orders tOrders


func (_ tOrders) Participate(
		eventid int,
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "eventid", eventid)
	return revel.MainRouter.Reverse("Orders.Participate", args).Url
}

func (_ tOrders) PlaceOrder(
		eventid int,
		pizza string,
		beverage string,
		qty int,
		addition []string,
		removals []string,
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "eventid", eventid)
	revel.Unbind(args, "pizza", pizza)
	revel.Unbind(args, "beverage", beverage)
	revel.Unbind(args, "qty", qty)
	revel.Unbind(args, "addition", addition)
	revel.Unbind(args, "removals", removals)
	return revel.MainRouter.Reverse("Orders.PlaceOrder", args).Url
}

func (_ tOrders) DeleteOrder(
		eventid int,
		orderid int,
		pizza string,
		beverage string,
		qty int,
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "eventid", eventid)
	revel.Unbind(args, "orderid", orderid)
	revel.Unbind(args, "pizza", pizza)
	revel.Unbind(args, "beverage", beverage)
	revel.Unbind(args, "qty", qty)
	return revel.MainRouter.Reverse("Orders.DeleteOrder", args).Url
}

func (_ tOrders) IgnoreOrder(
		id int,
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "id", id)
	return revel.MainRouter.Reverse("Orders.IgnoreOrder", args).Url
}

func (_ tOrders) RestoreOrder(
		id int,
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "id", id)
	return revel.MainRouter.Reverse("Orders.RestoreOrder", args).Url
}

func (_ tOrders) PayOrder(
		id int,
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "id", id)
	return revel.MainRouter.Reverse("Orders.PayOrder", args).Url
}

func (_ tOrders) UnpayOrder(
		id int,
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "id", id)
	return revel.MainRouter.Reverse("Orders.UnpayOrder", args).Url
}


type tUserController struct {}
var UserController tUserController


func (_ tUserController) ListUsers(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("UserController.ListUsers", args).Url
}

func (_ tUserController) EditUserView(
		username string,
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "username", username)
	return revel.MainRouter.Reverse("UserController.EditUserView", args).Url
}

func (_ tUserController) ChangeUserPsw(
		username string,
		psw1 string,
		psw2 string,
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "username", username)
	revel.Unbind(args, "psw1", psw1)
	revel.Unbind(args, "psw2", psw2)
	return revel.MainRouter.Reverse("UserController.ChangeUserPsw", args).Url
}

func (_ tUserController) EditUser(
		username string,
		role string,
		email string,
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "username", username)
	revel.Unbind(args, "role", role)
	revel.Unbind(args, "email", email)
	return revel.MainRouter.Reverse("UserController.EditUser", args).Url
}

func (_ tUserController) DeleteUser(
		username string,
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "username", username)
	return revel.MainRouter.Reverse("UserController.DeleteUser", args).Url
}

func (_ tUserController) RestoreUser(
		username string,
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "username", username)
	return revel.MainRouter.Reverse("UserController.RestoreUser", args).Url
}

func (_ tUserController) CreateUserView(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("UserController.CreateUserView", args).Url
}

func (_ tUserController) CreateUser(
		username string,
		role string,
		email string,
		psw1 string,
		psw2 string,
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "username", username)
	revel.Unbind(args, "role", role)
	revel.Unbind(args, "email", email)
	revel.Unbind(args, "psw1", psw1)
	revel.Unbind(args, "psw2", psw2)
	return revel.MainRouter.Reverse("UserController.CreateUser", args).Url
}


type tElements struct {}
var Elements tElements


func (_ tElements) ListIngredients(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("Elements.ListIngredients", args).Url
}

func (_ tElements) EditIngredientView(
		id int,
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "id", id)
	return revel.MainRouter.Reverse("Elements.EditIngredientView", args).Url
}

func (_ tElements) EditIngredient(
		id int,
		name string,
		price string,
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "id", id)
	revel.Unbind(args, "name", name)
	revel.Unbind(args, "price", price)
	return revel.MainRouter.Reverse("Elements.EditIngredient", args).Url
}

func (_ tElements) DeleteIngredient(
		id int,
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "id", id)
	return revel.MainRouter.Reverse("Elements.DeleteIngredient", args).Url
}

func (_ tElements) CreateIngredientView(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("Elements.CreateIngredientView", args).Url
}

func (_ tElements) CreateIngredient(
		name string,
		price string,
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "name", name)
	revel.Unbind(args, "price", price)
	return revel.MainRouter.Reverse("Elements.CreateIngredient", args).Url
}

func (_ tElements) GetPizzaIngredientsInJson(
		id int,
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "id", id)
	return revel.MainRouter.Reverse("Elements.GetPizzaIngredientsInJson", args).Url
}

func (_ tElements) ListPizza(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("Elements.ListPizza", args).Url
}

func (_ tElements) AddPizzaView(
		) string {
	args := make(map[string]string)
	
	return revel.MainRouter.Reverse("Elements.AddPizzaView", args).Url
}

func (_ tElements) AddPizza(
		name string,
		pizzatype string,
		price string,
		basepizza string,
		ingredients []string,
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "name", name)
	revel.Unbind(args, "pizzatype", pizzatype)
	revel.Unbind(args, "price", price)
	revel.Unbind(args, "basepizza", basepizza)
	revel.Unbind(args, "ingredients", ingredients)
	return revel.MainRouter.Reverse("Elements.AddPizza", args).Url
}

func (_ tElements) EditPizzaView(
		id int,
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "id", id)
	return revel.MainRouter.Reverse("Elements.EditPizzaView", args).Url
}

func (_ tElements) EditPizza(
		id int,
		name string,
		pizzatype string,
		price string,
		basepizza string,
		ingredients []string,
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "id", id)
	revel.Unbind(args, "name", name)
	revel.Unbind(args, "pizzatype", pizzatype)
	revel.Unbind(args, "price", price)
	revel.Unbind(args, "basepizza", basepizza)
	revel.Unbind(args, "ingredients", ingredients)
	return revel.MainRouter.Reverse("Elements.EditPizza", args).Url
}

func (_ tElements) DeletePizza(
		id int,
		) string {
	args := make(map[string]string)
	
	revel.Unbind(args, "id", id)
	return revel.MainRouter.Reverse("Elements.DeletePizza", args).Url
}


