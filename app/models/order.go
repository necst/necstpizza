package models

import (
	"crypto/md5"
	"errors"
	"fmt"
	"github.com/go-gorp/gorp"
	"io"
	"strings"
)

type PizzaOrder struct {
	OrderId    int `db_key:"true" db_autoincrement:"true"`
	EventId    int
	UserId     string `db_size:"20"`
	PizzaId    int
	BeverageId int
	Qty        int
	Ignore     bool //`db:"IgnoreOrder"`
	Paid       bool

	// Transient
	Event     *Event        `db_transient:"true"`
	User      *User         `db_transient:"true"`
	Pizza     *Pizza        `db_transient:"true"`
	Drink     *Beverage     `db_transient:"true"`
	Additions []*Ingredient `db_transient:"true"`
	Removals  []*Ingredient `db_transient:"true"`
}

type PizzaOrderAddition struct {
	OrderId      int `db_key:"true"`
	IngredientId int `db_key:"true"`
}

type PizzaOrderRemoval struct {
	OrderId      int `db_key:"true"`
	IngredientId int `db_key:"true"`
}

func (o *PizzaOrder) PreInsert(_ gorp.SqlExecutor) error {
	o.EventId = o.Event.EventId
	o.UserId = o.User.Username
	o.PizzaId = o.Pizza.Id
	o.BeverageId = o.Drink.Id

	return nil
}

func (p *PizzaOrder) PostInsert(sql gorp.SqlExecutor) error {
	// invalidAdds := make([]int, 0)
	// invalidRems := make([]int, 0)
	adds := make([]*Ingredient, 0)
	rems := make([]*Ingredient, 0)

	// validate additions
	for _, v := range p.Additions {
		if !p.Pizza.Contains(v) {
			adds = append(adds, v)
		}
	}

	// validate removals
	for _, v := range p.Removals {
		if p.Pizza.Contains(v) {
			rems = append(rems, v)
		}
	}

	for _, a := range adds {
		tmp := PizzaOrderAddition{p.OrderId, a.IngredientId}
		err := sql.Insert(&tmp)
		if err != nil {
			return err
		}
	}
	for _, a := range rems {
		tmp := PizzaOrderRemoval{p.OrderId, a.IngredientId}
		err := sql.Insert(&tmp)
		if err != nil {
			return err
		}
	}
	return nil
}

func (o *PizzaOrder) PostGet(sql gorp.SqlExecutor) error {
	var tmp interface{}
	var err error

	tmp, err = sql.Get(Event{}, o.EventId)
	if err != nil {
		return err
	}
	o.Event = tmp.(*Event)

	tmp, err = sql.Get(User{}, o.UserId)
	if err != nil {
		return err
	}
	o.User = tmp.(*User)

	tmp, err = sql.Get(Pizza{}, o.PizzaId)
	if err != nil {
		return err
	}
	o.Pizza = tmp.(*Pizza)

	tmp, err = sql.Get(Beverage{}, o.BeverageId)
	if err != nil {
		return err
	}
	o.Drink = tmp.(*Beverage)

	var (
		obj      []*PizzaOrderAddition
		objMinus []*PizzaOrderRemoval
		ingr     interface{}
	)

	// get order additions
	_, err = sql.Select(&obj, "select * from PizzaOrderAddition where OrderId = ?", o.OrderId)
	if err != nil {
		return errors.New(fmt.Sprintf("Error loading additions for (%d): %s", o.OrderId, err))
	}

	for _, v := range obj {
		ingr, err = sql.Get(&Ingredient{}, v.IngredientId)
		if err != nil {
			return errors.New(fmt.Sprintf("Error loading an ingredient (%d): %s", v, err))
		}
		if ingr != nil {
			o.Additions = append(o.Additions, ingr.(*Ingredient))
		}
	}

	// get order removals
	_, err = sql.Select(&objMinus, "select * from PizzaOrderRemoval where OrderId = ?", o.OrderId)
	if err != nil {
		return errors.New(fmt.Sprintf("Error loading removals for (%d): %s", o.OrderId, err))
	}

	for _, v := range objMinus {
		ingr, err = sql.Get(&Ingredient{}, v.IngredientId)
		if err != nil {
			return errors.New(fmt.Sprintf("Error loading an ingredient (%d): %s", v, err))
		}
		if ingr != nil {
			o.Removals = append(o.Removals, ingr.(*Ingredient))
		}
	}

	return nil
}

func (o *PizzaOrder) AdditionList() string {
	vals := make([]string, len(o.Additions))
	for i, v := range o.Additions {
		vals[i] = v.Name
	}
	return strings.Join(vals, ", ")
}

func (o *PizzaOrder) RemovalList() string {
	vals := make([]string, len(o.Removals))
	for i, v := range o.Removals {
		vals[i] = v.Name
	}
	return strings.Join(vals, ", ")
}

func (o *PizzaOrder) Price(special bool) float32 {
	var price float32
	if !special {
		price = o.Pizza.StandardPrice()
	} else {
		price = o.Pizza.SpecialPrice()
	}
	for _, v := range o.Additions {
		price += v.Price
	}

	if special {
		if price <= 5.0 {
			return 5.0 * float32(o.Qty)
		} else if price <= 6.5 {
			return 6.5 * float32(o.Qty)
		} else {
			return price * float32(o.Qty)
		}
	}
	return price * float32(o.Qty)
}

func (o *PizzaOrder) Hash() string {
	h := md5.New()
	// sortedIng := o.Additions
	//sort.Sort(&IngredientSorter{sortedIng})
	io.WriteString(h, o.Pizza.SpecialName())
	for _, v := range o.Additions {
		io.WriteString(h, "+"+v.Name)
	}
	for _, v := range o.Removals {
		io.WriteString(h, "-"+v.Name)
	}
	return fmt.Sprintf("%x", h.Sum(nil))
}
