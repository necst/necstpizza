package models

import (
	"fmt"
	"github.com/go-gorp/gorp"
	"strings"
)

type Ingredient struct {
	IngredientId int    `db_key:"true" db_autoincrement:"true"`
	Name         string `db_size:"30"`
	Price        float32
}

type PizzaToIngredient struct {
	PizzaId      int `db_key:"true"`
	IngredientId int `db_key:"true"`
}

type Pizza struct {
	Id     int    `db_key:"true" db_autoincrement:"true" form_options:"skip"`
	Name   string `db_size:"30"`
	BaseId int
	Price  float32

	// Transient
	Base  *Pizza        `db_transient:"true"`
	Ingrs []*Ingredient `db_transient:"true" form_options:"skip"`
}

type PizzaSorter struct {
	pizza []interface{}
}

func NewPizzaSorter(pi []interface{}) *PizzaSorter {
	return &PizzaSorter{pi}
}

func (p *PizzaSorter) Len() int {
	return len(p.pizza)
}

func (p *PizzaSorter) Swap(i, j int) {
	p.pizza[i], p.pizza[j] = p.pizza[j], p.pizza[i]
}

func (p *PizzaSorter) Less(i, j int) bool {
	return p.pizza[i].(*Pizza).Name < p.pizza[j].(*Pizza).Name
}

type IngredientSorter struct {
	ingrs []interface{}
}

func NewIngredientSorter(ingrs []interface{}) *IngredientSorter {
	return &IngredientSorter{ingrs}
}

func (i *IngredientSorter) Len() int {
	return len(i.ingrs)
}

func (ing *IngredientSorter) Swap(i, j int) {
	ing.ingrs[i], ing.ingrs[j] = ing.ingrs[j], ing.ingrs[i]
}

func (ing *IngredientSorter) Less(i, j int) bool {
	return ing.ingrs[i].(*Ingredient).Name < ing.ingrs[j].(*Ingredient).Name
}

type Beverage struct {
	Id   int    `db_key:"true" db_autoincrement:"true"`
	Name string `db_size:"30"`
}

func (p *Pizza) PreInsert(exe gorp.SqlExecutor) error {
	if p.Base != nil {
		if p.Base.Base == nil {
			p.BaseId = p.Base.Id
		} else {
			return fmt.Errorf("Cannot use a custom pizza as base.")
		}
	} else {
		p.BaseId = 0
	}
	return nil
}

func (p *Pizza) PostInsert(exe gorp.SqlExecutor) error {
	for i := range p.Ingrs {
		if p.Base == nil || !p.Base.Contains(p.Ingrs[i]) {
			tmp := &PizzaToIngredient{p.Id, p.Ingrs[i].IngredientId}
			if err := exe.Insert(tmp); err != nil {
				return err
			}
		}
	}
	return nil
}

func (p *Pizza) PreUpdate(exe gorp.SqlExecutor) error {
	_, err := exe.Exec("delete from PizzaToIngredient where PizzaId = ?", p.Id)
	if err != nil {
		return err
	}
	return p.PreInsert(exe)
}

func (p *Pizza) PostUpdate(exe gorp.SqlExecutor) error {
	return p.PostInsert(exe)
}

func (p *Pizza) PostGet(exe gorp.SqlExecutor) error {
	var (
		obj  []*PizzaToIngredient
		err  error
		ingr interface{}
	)

	// base pizza
	tmp, err := exe.Get(Pizza{}, p.BaseId)
	if err != nil {
		return fmt.Errorf("Error getting base pizza: %s", p.BaseId)
	}
	if tmp == nil {
		p.Base = nil
	} else {
		p.Base = tmp.(*Pizza)
	}

	// ingredients
	_, err = exe.Select(&obj, "select * from PizzaToIngredient where PizzaId = ?", p.Id)
	if err != nil {
		return fmt.Errorf("Error loading ingredients for (%d): %s", p.Id, err)
	}

	for _, v := range obj {
		ingr, err = exe.Get(&Ingredient{}, v.IngredientId)
		if err != nil {
			return fmt.Errorf("Error loading an ingredient (%d): %s", v, err)
		}
		p.Ingrs = append(p.Ingrs, ingr.(*Ingredient))
	}

	return nil
}

func (p *Pizza) PreDelete(exe gorp.SqlExecutor) error {
	err := p.PreUpdate(exe)
	if err != nil {
		return err
	}
	_, err = exe.Exec("delete from PizzaOrder where PizzaId = ?", p.Id)
	return err
}

func (i *Ingredient) PreDelete(exe gorp.SqlExecutor) error {
	_, err := exe.Exec("delete from PizzaToIngredient where IngredientId = ?", i.IngredientId)
	if err != nil {
		panic(err)
	}
	return nil
}

func (p *Pizza) StandardPrice() float32 {
	if p.Base == nil {
		return p.Price
	} else {
		baseP := p.Base.StandardPrice()
		for _, v := range p.Ingrs {
			baseP += v.Price
		}
		return baseP
	}
}

func (p *Pizza) SpecialPrice() float32 {
	var baseP float32
	if p.Base == nil {
		baseP = p.Price
	} else {
		baseP = p.Base.SpecialPrice()
		for _, v := range p.Ingrs {
			baseP += v.Price
		}
	}
	if baseP < 5.0 {
		return baseP
	} else if baseP < 6.5 {
		return 5.0
	}
	return 6.5
}

func (p *Pizza) Contains(ingr *Ingredient) bool {
	if p.Base != nil {
		if p.Base.Contains(ingr) {
			return true
		}
	}
	for _, v := range p.Ingrs {
		if v.IngredientId == ingr.IngredientId {
			return true
		}
	}
	return false
}

func (p *Pizza) IngredientsNames() []string {
	ret := make([]string, 0)
	// if p.Base != nil {
	// 	for _, v := range p.Base.Ingrs {
	// 		ret = append(ret, v.Name)
	// 	}
	// }
	for _, v := range p.Ingrs {
		ret = append(ret, v.Name)
	}
	return ret
}

func (p *Pizza) Ingredients() []*Ingredient {
	ingrs := make([]*Ingredient, 0)
	if p.Base != nil {
		for _, v := range p.Base.Ingrs {
			ingrs = append(ingrs, v)
		}
	}
	for _, v := range p.Ingrs {
		ingrs = append(ingrs, v)
	}
	return ingrs
}

func (p *Pizza) SpecialName() string {
	if p.Base != nil {
		return fmt.Sprintf("(%s)+%s", p.Base.Name, strings.Join(p.IngredientsNames(), ","))
	} else {
		return p.Name
	}
}
