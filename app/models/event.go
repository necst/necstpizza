package models

import (
	"fmt"
	"github.com/go-gorp/gorp"
	"time"
)

const (
	DATE_FORMAT     = "Jan _2, 2006 - 15:04"
	SQL_DATE_FORMAT = "2006-01-02 15:04"
)

type Event struct {
	EventId                int    `db_key:"true" db_autoincrement:"true"`
	Name                   string `db_size:"30"`
	ManagerId              string
	Description            string `db_size:"1024"`
	AdditionalInfo         string `db_size:"4096"`
	TimeStampString        string `db_size:"30"`
	RecurrenceInt          int
	CreatorId              string `db_size:"100"`
	ParentId               int    `db_size:"30"`
	NotificationTimeString string
	Active                 bool
	Notified               bool
	CreateChildSince       string
	SpecialPrice           bool

	// Transient
	Timestamp            time.Time     `db_transient:"true"`
	Recurrence           time.Duration `db_transient:"true"`
	RecurrenceParent     *Event        `db_transient:"true"`
	NotificationTime     time.Time     `db_transient:"true"`
	CreateChildSinceTime time.Time     `db_transient:"true"`
}

type RecurrenceChild struct {
	ParentId int `db_key:"true"`
	ChildId  int `db_key:"true"`
}

type EventSorter struct {
	events []interface{}
}

func NewEventSorter(ev []interface{}) *EventSorter {
	return &EventSorter{ev}
}

func (e *EventSorter) Len() int {
	return len(e.events)
}

func (e *EventSorter) Swap(i, j int) {
	e.events[i], e.events[j] = e.events[j], e.events[i]
}

func (e *EventSorter) Less(i, j int) bool {
	return e.events[i].(*Event).NotificationTime.Before(e.events[j].(*Event).NotificationTime)
}

func (e *Event) String() string {
	return fmt.Sprintf("%s ")
}

func (e *Event) PreInsert(_ gorp.SqlExecutor) error {
	e.TimeStampString = e.Timestamp.Format(SQL_DATE_FORMAT)

	e.RecurrenceInt = int(e.Recurrence.Hours())

	if e.RecurrenceParent != nil {
		e.ParentId = e.RecurrenceParent.EventId
	} else {
		e.ParentId = -1
	}

	e.CreateChildSinceTime = time.Now()
	e.NotificationTimeString = e.NotificationTime.Format(SQL_DATE_FORMAT)
	e.CreateChildSince = e.CreateChildSinceTime.Format(SQL_DATE_FORMAT)

	return nil
}

func (e *Event) PreUpdate(sql gorp.SqlExecutor) error {
	return e.PreInsert(sql)
}

func (e *Event) PostGet(sql gorp.SqlExecutor) error {
	var err error
	if e.Timestamp, err = time.ParseInLocation(SQL_DATE_FORMAT, e.TimeStampString, time.Local); err != nil {
		return err
	}

	if e.RecurrenceInt > 0 {
		if e.Recurrence, err = time.ParseDuration(fmt.Sprintf("%dh", e.RecurrenceInt)); err != nil {
			return err
		}
	} else {
		e.Recurrence = 0
	}

	if e.ParentId != -1 {
		obj, err := sql.Get(Event{}, e.ParentId)
		if err != nil {
			return err
		}
		e.RecurrenceParent = obj.(*Event)
	} else {
		e.RecurrenceParent = nil
	}

	if e.NotificationTime, err = time.ParseInLocation(SQL_DATE_FORMAT, e.NotificationTimeString, time.Local); err != nil {
		return err
	}

	if e.CreateChildSinceTime, err = time.ParseInLocation(SQL_DATE_FORMAT, e.CreateChildSince, time.Local); err != nil {
		return err
	}

	return nil
}

func (e *Event) PreDelete(exe gorp.SqlExecutor) error {
	_, err := exe.Exec("delete from PizzaOrder where EventId = ?", e.EventId)
	_, err = exe.Exec("delete from RecurrenceChild where ParentId = ? or ChildId = ?", e.EventId, e.EventId)
	return err
}

func (e *Event) Eta() time.Duration {
	diff := e.NotificationTime.Sub(time.Now())
	diff = time.Duration(int64((diff / time.Second) * time.Second))
	return diff
}

func (e *Event) InTime() bool {
	return e.Timestamp.After(time.Now())
}

func (e *Event) Open() bool {
	return e.NotificationTime.After(time.Now())
}

func (e *Event) Recurrent() bool {
	return e.RecurrenceInt > 0
}
