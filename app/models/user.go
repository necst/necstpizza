package models

import (
	"fmt"
	"github.com/go-gorp/gorp"
	"strconv"
)

const (
	ADMIN = iota
	USER
)

type User struct {
	Username       string `db_key:"true" db_size:"20"`
	Role           int
	Email          string `db_size:"50"`
	Admin          bool   `db_transient:"true"`
	Password       string `db_transient:"true"`
	HashedPassword []byte
	Enabled        bool
}

func (u *User) String() string {
	if u.Enabled {
		// if u.Admin {
		// 	return fmt.Sprintf("%s*", u.Username)
		// } else {
		// 	return u.Username
		// }
		return u.Username
	} else {
		return fmt.Sprintf("#%s#", u.Username)
	}
}

func (u *User) PostGet(_ gorp.SqlExecutor) error {
	switch u.Role {
	case ADMIN:
		u.Admin = true
	case USER:
		u.Admin = false
	default:
		u.Admin = false
	}
	return nil
}

func (u *User) SetRole(r string) {
	if rl, err := strconv.ParseInt(r, 10, 0); err == nil {
		u.Role = int(rl)
	}
}

func (u *User) Disable() {
	u.Enabled = false
}

func (u *User) Enable() {
	u.Enabled = true
}
