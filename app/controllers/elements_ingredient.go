package controllers

import (
	"fmt"
	"github.com/kirves/go-form-it"
	"github.com/kirves/go-form-it/fields"
	"github.com/revel/revel"
	"necstpizza/app/models"
	"necstpizza/app/routes"
	"sort"
	"strconv"
)

var (
	ingredientOpts map[string][]fields.InputChoice = map[string][]fields.InputChoice{
		"": []fields.InputChoice{
			fields.InputChoice{"0", "--Free--"},
			fields.InputChoice{"1", "1 euro"},
			fields.InputChoice{"1.5", "1.5 euros"},
		},
	}
)

func (c Elements) ListIngredients() revel.Result {
	u := c.connected()
	if !u.Admin {
		return c.Forbidden("")
	}
	ingrs, err := c.Txn.Select(models.Ingredient{}, "select * from Ingredient")
	if err != nil {
		panic(err)
	}
	sort.Sort(models.NewIngredientSorter(ingrs))
	c.ViewArgs["ingrs"] = ingrs
	return c.Render()
}

func (c Elements) EditIngredientView(id int) revel.Result {
	u := c.connected()
	if !u.Admin {
		return c.Forbidden("")
	}

	tmp, err := c.Txn.Get(models.Ingredient{}, id)
	if err != nil {
		panic(err)
	}
	if tmp == nil {
		return c.NotFound("")
	}
	ingr := tmp.(*models.Ingredient)

	nameVal := ingr.Name
	if c.Flash.Data["name"] != "" {
		nameVal = c.Flash.Data["name"]
	}
	form := c.createIngredientForm(revel.MainRouter.Reverse("Elements.EditIngredient", nil).URL)
	form.Field("name").SetValue(nameVal)
	form.Field("price").SetValue(c.Flash.Data["price"])

	form.Elements(
		fields.HiddenField("id").SetValue(fmt.Sprintf("%d", id)),
		fields.SubmitButton("submit", "Save"),
		fields.Button("delete", "Delete").AddClass("btn-danger").SetParam("onClick", fmt.Sprintf("location.href='%s'", routes.Elements.DeleteIngredient(id))),
	)

	c.ViewArgs["form"] = form
	return c.Render()
}

func (c Elements) EditIngredient(id int, name string, price string) revel.Result {
	u := c.connected()
	if !u.Admin {
		return c.Forbidden("")
	}
	c.validateIngredient(name, price)
	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(routes.Elements.EditIngredientView(id))
	}

	tmp, err := c.Txn.Get(models.Ingredient{}, id)
	if err != nil {
		panic(err)
	}
	if tmp == nil {
		return c.NotFound("")
	}
	ingr := tmp.(*models.Ingredient)
	ingr.Name = name
	prVal, _ := strconv.ParseFloat(price, 32)
	ingr.Price = float32(prVal)

	_, err = c.Txn.Update(ingr)
	if err != nil {
		panic(err)
	}

	c.Flash.Success("Successfully edited ingredient: %s", ingr.Name)
	return c.Redirect(routes.Elements.ListIngredients())
}

func (c Elements) DeleteIngredient(id int) revel.Result {
	u := c.connected()
	if !u.Admin {
		return c.Forbidden("")
	}

	tmp, err := c.Txn.Get(models.Ingredient{}, id)
	if err != nil {
		panic(err)
	}
	if tmp == nil {
		return c.NotFound("")
	}
	ingr := tmp.(*models.Ingredient)

	_, err = c.Txn.Delete(ingr)
	if err != nil {
		panic(err)
	}

	c.Flash.Success("Successfully deleted ingredient: %s", ingr.Name)
	return c.Redirect(routes.Elements.ListIngredients())
}

func (c Elements) CreateIngredientView() revel.Result {
	u := c.connected()
	if !u.Admin {
		return c.Forbidden("")
	}
	form := c.createIngredientForm(revel.MainRouter.Reverse("Elements.CreateIngredient", nil).URL)
	form.Field("name").SetValue(c.Flash.Data["name"])
	form.Field("price").SetValue(c.Flash.Data["price"])
	form.Elements(
		fields.SubmitButton("submit", "Create"),
	)

	c.ViewArgs["form"] = form
	return c.Render()
}

func (c Elements) CreateIngredient(name string, price string) revel.Result {
	u := c.connected()
	if !u.Admin {
		return c.Forbidden("")
	}

	c.validateIngredient(name, price)
	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(routes.Elements.CreateIngredientView())
	}

	prVal, _ := strconv.ParseFloat(price, 32)
	ingr := models.Ingredient{0, name, float32(prVal)}
	err := c.Txn.Insert(&ingr)
	if err != nil {
		panic(err)
	}

	c.Flash.Success("Successfully created ingredient: %s", ingr.Name)
	return c.Redirect(routes.Elements.ListIngredients())
}

func (c Elements) createIngredientForm(action string) *forms.Form {
	form := forms.BootstrapForm(forms.POST, action).Elements(
		fields.TextField("name").SetLabel("Name"),
		fields.SelectField("price", ingredientOpts).SetLabel("Price as addition"),
	)

	if c.Validation.HasErrors() {
		for k, err := range c.Validation.ErrorMap() {
			form.Field(k).AddError(err.Message)
		}
	}

	return form
}

func (c Elements) validateIngredient(name, price string) {
	c.Validation.Required(name).Message("Name is required.")
	c.Validation.MaxSize(name, 30).Message("Name cannot have more than 30 characters.")
	c.Validation.Required(price).Message("Invalid entry.")

	found := false
	for _, v := range ingredientOpts[""] {
		if price == v.Id {
			found = true
		}
	}

	if !found {
		c.Validation.Error("Invalid entry.").Key("price")
	}
}
