package controllers

import (
	"github.com/revel/revel"
	"necstpizza/app/routes"
)

type Elements struct {
	App
}

func (c Elements) checkUser() revel.Result {
	user := c.connected()
	if user == nil {
		return c.Redirect(routes.App.Index())
	}
	if user.Email == "" {
		c.Flash.Error("Before moving on please insert add your email address.")
		return c.Redirect(routes.UserController.EditUserView(user.Username))
	}
	return nil
}
