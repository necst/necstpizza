package controllers

import (
	"fmt"
	"github.com/kirves/go-form-it"
	"github.com/kirves/go-form-it/fields"
	"github.com/revel/revel"
	"golang.org/x/crypto/bcrypt"
	"necstpizza/app/models"
	"necstpizza/app/routes"
)

type UserController struct {
	App
}

func (c UserController) checkUser() revel.Result {
	user := c.connected()
	if user == nil {
		return c.Redirect(routes.App.Index())
	}
	// if user.Email == "" {
	// 	c.Flash.Error("Before moving on please insert add your email address.")
	// 	return c.Redirect(routes.UserController.EditUserView(user.Username))
	// }
	return nil
}

func (c UserController) ListUsers() revel.Result {
	u := c.connected()
	if !u.Admin {
		return c.Forbidden("")
	}
	users, err := c.Txn.Select(models.User{}, "select * from User")
	if err != nil {
		panic(err)
	}

	c.ViewArgs["users"] = users
	return c.Render()
}

func (c UserController) EditUserView(username string) revel.Result {
	u := c.connected()
	if !u.Admin && username != u.Username {
		return c.Forbidden("")
	}
	tmp, err := c.Txn.Get(models.User{}, username)
	if err != nil {
		panic(err)
	}
	if tmp == nil {
		return c.NotFound("")
	}
	user := tmp.(*models.User)

	roleVal := c.Flash.Data["role"]
	if roleVal == "" {
		roleVal = fmt.Sprintf("%d", user.Role)
	}

	emailVal := c.Flash.Data["email"]
	if emailVal == "" {
		emailVal = user.Email
	}

	basic_form := forms.BootstrapForm(forms.POST, revel.MainRouter.Reverse("UserController.EditUser", nil).URL).Elements(
		fields.StaticField("username_static", user.Username).SetLabel("Username"),
		fields.HiddenField("username").SetValue(user.Username),
	)
	if username != u.Username {
		basic_form.Elements(
			fields.SelectField("role", map[string][]fields.InputChoice{
				"": []fields.InputChoice{
					fields.InputChoice{"0", "Admin"},
					fields.InputChoice{"1", "Normal user"},
				},
			}).SetLabel("Role").SetValue(roleVal),
		)
	}
	basic_form.Elements(
		fields.TextField("email").SetLabel("E-mail").SetValue(emailVal),
	)

	if !user.Enabled {
		basic_form.Field("role").Disabled()
		basic_form.Field("email").Disabled()
		basic_form.Elements(
			fields.Button("restore", "Restore").AddClass("btn-danger").SetParam(
				"onClick",
				fmt.Sprintf("location.href='%s'",
					routes.UserController.RestoreUser(username))),
		)
	} else {
		basic_form.Elements(
			fields.SubmitButton("submit", "Save"),
		)
		if username != u.Username {
			basic_form.Elements(
				fields.Button("delete", "Deactivate").AddClass("btn-danger").SetParam(
					"onClick",
					fmt.Sprintf("location.href='%s'",
						routes.UserController.DeleteUser(username))),
			)
		}
	}

	psw_form := forms.BootstrapForm(forms.POST, revel.MainRouter.Reverse("UserController.ChangeUserPsw", nil).URL).Elements(
		fields.StaticField("username_static", user.Username).SetLabel("Username"),
		fields.HiddenField("username").SetValue(user.Username),
		fields.PasswordField("psw1").SetLabel("Password").SetValue(c.Flash.Data["psw1"]),
		fields.PasswordField("psw2").SetLabel("Verify password"),
		fields.SubmitButton("submit", "Change password"),
	)

	if !user.Enabled {
		psw_form.Field("psw1").Disabled()
		psw_form.Field("psw2").Disabled()
		psw_form.Field("submit").Disabled()
	}

	if c.Validation.HasErrors() {
		for k, err := range c.Validation.ErrorMap() {
			basic_form.Field(k).AddError(err.Message)
			psw_form.Field(k).AddError(err.Message)
		}
	}

	c.ViewArgs["basic_form"] = basic_form
	c.ViewArgs["psw_form"] = psw_form
	return c.Render()
}

func (c UserController) ChangeUserPsw(username, psw1, psw2 string) revel.Result {
	u := c.connected()
	if !u.Admin && username != u.Username {
		return c.Forbidden("")
	}
	tmp, err := Dbm.Get(models.User{}, username)
	if err != nil {
		panic(err)
	}
	if tmp == nil {
		return c.NotFound("")
	}

	user := tmp.(*models.User)
	if !user.Enabled {
		return c.Forbidden("")
	}
	c.Validation.Required(psw1).Key("psw1").Message("Password must have at least 5 characters.")
	c.Validation.MinSize(psw1, 5).Key("psw1").Message("Password must have at least 5 characters.")
	c.Validation.Required(psw1 == psw2).Key("psw2").Message("Passwords do not match.")

	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(routes.UserController.EditUserView(username))
	}

	// ok, we can save new user psw
	user.HashedPassword, _ = bcrypt.GenerateFromPassword([]byte(psw1), bcrypt.DefaultCost)
	_, err = c.Txn.Update(user)
	if err != nil {
		panic(err)
	}

	c.Flash.Success("Successuflly changed password for user: %s", user.String())
	if u.Admin {
		return c.Redirect(routes.UserController.ListUsers())
	}
	return c.Redirect(routes.Events.CalendarView())
}

func (c UserController) EditUser(username, role, email string) revel.Result {
	u := c.connected()
	if !u.Admin && username != u.Username {
		return c.Forbidden("")
	}
	tmp, err := c.Txn.Get(models.User{}, username)
	if err != nil {
		panic(err)
	}
	if tmp == nil {
		return c.NotFound("")
	}

	user := tmp.(*models.User)
	if !user.Enabled {
		return c.Forbidden("")
	}
	if username != u.Username {
		c.Validation.Required(role).Message("A role must be defined.")
	}
	c.Validation.Email(email).Message("Invalid email address.")

	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(routes.UserController.EditUserView(username))
	}

	// ok, we can save new user psw
	if username != u.Username {
		user.SetRole(role)
	}
	user.Email = email
	_, err = c.Txn.Update(user)
	if err != nil {
		panic(err)
	}

	c.Flash.Success("Successuflly updated user: %s", user.String())
	if u.Admin {
		return c.Redirect(routes.UserController.ListUsers())
	}
	return c.Redirect(routes.Events.CalendarView())
}

func (c UserController) DeleteUser(username string) revel.Result {
	u := c.connected()
	if !u.Admin || username == u.Username {
		return c.Forbidden("")
	}

	tmp, err := c.Txn.Get(models.User{}, username)
	if err != nil {
		panic(err)
	}
	if tmp == nil {
		return c.NotFound("")
	}

	user := tmp.(*models.User)

	user.Disable()
	_, err = c.Txn.Update(user)
	if err != nil {
		panic(err)
	}

	c.Flash.Success("Successuflly deactivated user: %s", user.String())
	return c.Redirect(routes.UserController.ListUsers())
}

func (c UserController) RestoreUser(username string) revel.Result {
	u := c.connected()
	if !u.Admin || username == u.Username {
		return c.Forbidden("")
	}

	tmp, err := c.Txn.Get(models.User{}, username)
	if err != nil {
		panic(err)
	}
	if tmp == nil {
		return c.NotFound("")
	}

	user := tmp.(*models.User)
	user.Enable()
	_, err = c.Txn.Update(user)
	if err != nil {
		panic(err)
	}

	c.Flash.Success("Successuflly restored user: %s", user.String())
	return c.Redirect(routes.UserController.EditUserView(username))
}

func (c UserController) CreateUserView() revel.Result {
	u := c.connected()
	if !u.Admin {
		return c.Forbidden("")
	}

	roleVal := c.Flash.Data["role"]
	if roleVal == "" {
		roleVal = "1"
	}

	form := forms.BootstrapForm(forms.POST, revel.MainRouter.Reverse("UserController.CreateUser", nil).URL).Elements(
		fields.TextField("username").SetLabel("Username").SetValue(c.Flash.Data["username"]),
		fields.SelectField("role", map[string][]fields.InputChoice{
			"": []fields.InputChoice{
				fields.InputChoice{"0", "admin"},
				fields.InputChoice{"1", "normal user"},
			},
		}).SetLabel("Role").SetValue(roleVal),
		fields.TextField("email").SetLabel("E-mail").SetValue(c.Flash.Data["email"]),
		fields.PasswordField("psw1").SetLabel("Password").SetValue(c.Flash.Data["psw1"]),
		fields.PasswordField("psw2").SetLabel("Verify password"),
		fields.SubmitButton("submit", "Create"),
	)

	if c.Validation.HasErrors() {
		for k, err := range c.Validation.ErrorMap() {
			form.Field(k).AddError(err.Message)
		}
	}

	c.ViewArgs["form"] = form
	return c.Render()
}

func (c UserController) CreateUser(username, role, email, psw1, psw2 string) revel.Result {
	u := c.connected()
	if !u.Admin {
		return c.Forbidden("")
	}

	c.Validation.Required(username).Message("Missing username.")
	if user, _ := c.Txn.Get(models.User{}, username); user != nil {
		c.Validation.Error("Username already exists.").Key("username")
	}
	c.Validation.Required(role).Message("A role must be defined.")
	c.Validation.Email(email).Message("Invalid email address.")
	c.Validation.Required(psw1).Key("psw1").Message("Password must have at least 5 characters.")
	c.Validation.MinSize(psw1, 5).Key("psw1").Message("Password must have at least 5 characters.")
	c.Validation.Required(psw1 == psw2).Key("psw2").Message("Passwords do not match.")

	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(routes.UserController.CreateUserView())
	}

	user := &models.User{username, 1, email, false, psw1, []byte{}, true}
	user.SetRole(role)
	user.HashedPassword, _ = bcrypt.GenerateFromPassword([]byte(psw1), bcrypt.DefaultCost)
	err := c.Txn.Insert(user)
	if err != nil {
		panic(err)
	}

	c.Flash.Success("Successuflly created user: %s", user.String())
	return c.Redirect(routes.UserController.ListUsers())
}
