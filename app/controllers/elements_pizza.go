package controllers

import (
	"fmt"
	"github.com/kirves/go-form-it"
	"github.com/kirves/go-form-it/fields"
	"github.com/revel/revel"
	"necstpizza/app/models"
	"necstpizza/app/routes"
	"sort"
	"strconv"
)

func (c Elements) GetPizzaIngredientsInJson(id int) revel.Result {
	u := c.connected()
	if u == nil {
		return c.Forbidden("")
	}
	tmp, err := c.Txn.Get(models.Pizza{}, id)
	if err != nil {
		panic(err)
	}
	if tmp == nil {
		return c.NotFound("Not found")
	}

	pizza := tmp.(*models.Pizza)

	type TmpIngr struct {
		Id   int
		Name string
	}

	pizzaIngrs := pizza.Ingredients()
	ingrs := make([]TmpIngr, len(pizzaIngrs))
	for i, v := range pizzaIngrs {
		ingrs[i] = TmpIngr{v.IngredientId, v.Name}
	}

	return c.RenderJSON(ingrs)
}

func (c Elements) ListPizza() revel.Result {
	pizzas, err := c.Txn.Select(models.Pizza{}, "select * from Pizza")
	if err != nil {
		c.RenderError(err)
	}

	sort.Sort(models.NewPizzaSorter(pizzas))
	c.ViewArgs["pizzas"] = pizzas
	return c.Render()
}

func (c Elements) AddPizzaView() revel.Result {
	u := c.connected()
	if !u.Admin {
		return c.Forbidden("")
	}
	form := c.createPizzaForm(revel.MainRouter.Reverse("Elements.AddPizza", nil).URL)

	form.Field("name").SetValue(c.Flash.Data["name"])
	form.Field("custom").SetValue(c.Flash.Data["pizzatype"])
	form.Field("price").SetValue(c.Flash.Data["price"])
	form.Field("basepizza").SetValue(c.Flash.Data["basepizza"])
	form.Elements(
		fields.SubmitButton("submit", "Add"),
		fields.ResetButton("reset", "Reset"),
	)

	c.ViewArgs["form"] = form
	return c.Render()
}

func (c Elements) AddPizza(name string, pizzatype string, price string, basepizza string, ingredients []string) revel.Result {
	u := c.connected()
	if !u.Admin {
		return c.Forbidden("")
	}
	c.validatePizza(name, pizzatype, price, basepizza, ingredients)

	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(routes.Elements.AddPizzaView())
	}

	pizza := c.createPizzaInstance(name, pizzatype, price, basepizza, ingredients)
	err := c.Txn.Insert(pizza)
	if err != nil {
		panic(err)
	}

	c.Flash.Success("New pizza created: %s", name)
	return c.Redirect(routes.Elements.ListPizza())
}

func (c Elements) EditPizzaView(id int) revel.Result {
	u := c.connected()
	if !u.Admin {
		return c.Forbidden("")
	}
	tmp, err := c.Txn.Get(models.Pizza{}, id)
	if err != nil {
		panic(err)
	}
	if tmp == nil {
		return c.NotFound("")
	}
	pizza := tmp.(*models.Pizza)

	selected := make([]string, len(pizza.Ingrs))
	for i, v := range pizza.Ingrs {
		selected[i] = fmt.Sprintf("%d", v.IngredientId)
	}
	form := c.createPizzaForm(revel.MainRouter.Reverse("Elements.EditPizza", nil).URL)
	form.Field("name").SetValue(pizza.Name)
	if pizza.Base == nil {
		form.Field("custom").SetValue("0")
		form.Field("price").SetValue(fmt.Sprintf("%.2f", pizza.Price))
	} else {
		form.Field("custom").SetValue("1")
		form.Field("basepizza").SetValue(fmt.Sprintf("%d", pizza.Base.Id))
	}
	form.Field("ingredients").AddSelected(selected...)
	form.Elements(
		fields.HiddenField("id").SetValue(fmt.Sprintf("%d", pizza.Id)),
		fields.SubmitButton("submit", "Save"),
		fields.Button("reset", "Cancel").SetParam("onClick", fmt.Sprintf("location.href='%s'", revel.MainRouter.Reverse("Elements.ListPizza", nil).URL)),
		fields.Button("delete", "Delete").AddClass("btn-danger").SetParam(
			"onClick",
			fmt.Sprintf("location.href='%s'",
				routes.Elements.DeletePizza(pizza.Id))),
	)

	c.ViewArgs["form"] = form
	return c.Render()
}

func (c Elements) EditPizza(id int, name string, pizzatype, price string, basepizza string, ingredients []string) revel.Result {
	u := c.connected()
	if !u.Admin {
		return c.Forbidden("")
	}
	c.validatePizza(name, pizzatype, price, basepizza, ingredients)
	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(routes.Elements.EditPizzaView(id))
	}

	pizza := c.createPizzaInstance(name, pizzatype, price, basepizza, ingredients)
	pizza.Id = id
	_, err := c.Txn.Update(pizza)
	if err != nil {
		panic(err)
	}

	c.Flash.Success("Edited pizza: %s", name)
	return c.Redirect(routes.Elements.ListPizza())
}

func (c Elements) DeletePizza(id int) revel.Result {
	u := c.connected()
	if !u.Admin {
		return c.Forbidden("")
	}
	_, err := c.Txn.Delete(&models.Pizza{Id: id})
	if err != nil {
		panic(err)
	}
	return c.Redirect(routes.Elements.ListPizza())
}

func (c Elements) validatePizza(name, custom, price string, basepizza string, ingredients []string) {
	c.Validation.Required(name).Message("Pizza name is required.")
	c.Validation.MaxSize(name, 30).Message("Name can have a maximum of 30 characters.")
	if custom == "0" {
		c.Validation.Required(price).Message("Price is required.")
		if price == "" {
			c.Validation.Error("Price must be a decimal number.").Key("price")
		}
		pVal, err := strconv.ParseFloat(price, 32)
		if err != nil {
			c.Validation.Error("Price must be a decimal number.").Key("price")
		} else {
			c.Validation.Required(pVal >= 1.0).Key("price").Message("Price must be between 1.0 and 10")
			c.Validation.Required(pVal <= 10).Key("price").Message("Price must be between 1.0 and 10")
		}
	} else if custom == "1" {
		tmp, err := c.Txn.Get(models.Pizza{}, basepizza)
		if err != nil {
			panic(err)
		}
		if tmp == nil {
			c.Validation.Error("Selected base pizza does not exist.").Key("basepizza")
		}
	} else {
		c.Validation.Required(custom).Message("Pizza type must be selected.")
	}
	c.Validation.Required(ingredients).Message("At least one ingredient is required.")
}

func (c Elements) createPizzaInstance(name, custom, price string, basepizza string, ingredients []string) *models.Pizza {
	var pVal float64
	var pizza *models.Pizza

	if custom == "0" {
		pVal, _ = strconv.ParseFloat(price, 32)
		pizza = nil
	} else if custom == "1" {
		tmp, err := c.Txn.Get(models.Pizza{}, basepizza)
		if err != nil {
			panic(err)
		}
		pizza = tmp.(*models.Pizza)
		pVal = 0.0
	}

	// get ingredients
	ingrs := make([]*models.Ingredient, 0)
	for _, ing := range ingredients {
		val, err := strconv.ParseInt(ing, 10, 32)
		if err != nil {
			continue
		}
		tmp, err := c.Txn.Get(models.Ingredient{}, int(val))
		if err != nil {
			panic(err)
		}
		ingrs = append(ingrs, tmp.(*models.Ingredient))
	}

	// create model
	return &models.Pizza{0, name, pizza.Id, float32(pVal), pizza, ingrs}
}

func (c Elements) createPizzaForm(action string) *forms.Form {
	// pizzas
	tmp, err := Dbm.Select(models.Pizza{}, "select * from Pizza")
	if err != nil {
		panic(err)
	}
	sort.Sort(models.NewPizzaSorter(tmp))

	pizzaOpts := map[string][]fields.InputChoice{
		"": make([]fields.InputChoice, 0),
	}
	for _, p := range tmp {
		if p.(*models.Pizza).Base == nil {
			pizzaOpts[""] = append(pizzaOpts[""], fields.InputChoice{fmt.Sprintf("%d", p.(*models.Pizza).Id), p.(*models.Pizza).Name})
		}
	}

	// ingredients
	ingrs, err := c.Txn.Select(models.Ingredient{}, "select * from Ingredient")
	if err != nil {
		c.RenderError(err)
	}
	sort.Sort(models.NewIngredientSorter(ingrs))
	opts := map[string][]fields.InputChoice{"": make([]fields.InputChoice, 0)}
	for _, i := range ingrs {
		opts[""] = append(opts[""], fields.InputChoice{fmt.Sprintf("%d", i.(*models.Ingredient).IngredientId), i.(*models.Ingredient).Name})
	}
	form := forms.BootstrapForm(forms.POST, action).Elements(
		fields.TextField("name").SetLabel("Name"),
		fields.RadioField("custom", []fields.InputChoice{
			fields.InputChoice{"0", "Standard"},
			fields.InputChoice{"1", "Custom"},
		}).AddLabelClass("radio-inline").SetId("pizzatype"),
		fields.TextField("price").SetLabel("Price").AddClass("price-field").AddLabelClass("price-field"),
		fields.SelectField("basepizza", pizzaOpts).SetParam("title", "No base pizza").SetLabel("Base pizza").AddClass("base-field").AddLabelClass("base-field").SetValue("1"),
		fields.SelectField("ingredients", opts).MultipleChoice().SetLabel("Ingredients").SetParam("size", "20"),
	)

	if c.Validation.HasErrors() {
		for k, err := range c.Validation.ErrorMap() {
			form.Field(k).AddError(err.Message)
		}
	}

	return form
}
