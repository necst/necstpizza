package controllers

import (
	"bytes"
	"errors"
	"fmt"
	"github.com/revel/revel"
	"html/template"
	"necstpizza/app/models"
	"necstpizza/app/routes"
	"net/smtp"
)

type PizzaEntry struct {
	Pizza     template.HTML
	Additions string
	Removals  string
	Qty       int
}

func sendEmail(username, email string, content []byte) error {
	auth := smtp.PlainAuth(
		"",
		revel.Config.StringDefault("email.username", ""),
		revel.Config.StringDefault("email.password", ""),
		revel.Config.StringDefault("email.smtp_host", ""),
	)

	err := smtp.SendMail(
		fmt.Sprintf("%s:%s", revel.Config.StringDefault("email.smtp_host", ""), revel.Config.StringDefault("email.smtp_port", "25")),
		auth,
		"NECSTPizza Daemon",
		[]string{email},
		content,
	)

	if err != nil {
		return errors.New("SENDMAIL ERROR: " + err.Error())
	}

	return nil
}

func notifyMailingList(manager *models.User, event *models.Event) error {
	buf := bytes.Buffer{}
	var assEmail string = ""
	if manager != nil {
		assEmail = manager.Email
	} else {
		assEmail = revel.Config.StringDefault("app.technicalEmail", "")
	}
	context := map[string]interface{}{
		"To":            revel.Config.StringDefault("app.mailingList", ""),
		"url":           revel.Config.StringDefault("app.site", "http://127.0.0.1:9000") + routes.Orders.Participate(event.EventId),
		"event":         event,
		"info":          template.HTML(event.AdditionalInfo),
		"assigneeMail":  assEmail,
		"technicalMail": revel.Config.StringDefault("app.technicalEmail", ""),
	}

	tmpl, err := revel.MainTemplateLoader.Template("notifications/invitation.txt")
	if err != nil {
		return err
	}
	err = tmpl.Render(&buf, context)
	if err != nil {
		return err
	}

	return sendEmail("", revel.Config.StringDefault("app.mailingList", ""), buf.Bytes())
}

func notifyAssignee(username, email string, eventid int) error {
	buf := bytes.Buffer{}
	context := map[string]interface{}{
		"To":       email,
		"username": username,
		"url":      revel.Config.StringDefault("app.site", "http://127.0.0.1:9000") + routes.Orders.Participate(eventid),
	}

	tmpl, err := revel.MainTemplateLoader.Template("notifications/assignee.txt")
	if err != nil {
		return err
	}
	err = tmpl.Render(&buf, context)
	if err != nil {
		return err
	}

	return sendEmail(username, email, buf.Bytes())
}

func SendSummary(event *models.Event, user *models.User, pizzas map[string]*PizzaEntry, bevs map[string]int, totalList map[string]float32, total float32, orderList map[string]map[string]*PizzaEntry) error {
	buf := bytes.Buffer{}
	context := map[string]interface{}{
		"To":       user.Email,
		"username": user.Username,
		"url":      revel.Config.StringDefault("app.site", "http://127.0.0.1:9000") + routes.Orders.Participate(event.EventId),
		"event":    event,
		"pizzas":   pizzas,
		"bevs":     bevs,
		"totals":   totalList,
		"tot":      total,
		"orders":   orderList,
	}

	tmpl, err := revel.MainTemplateLoader.Template("notifications/summary.txt")
	if err != nil {
		return err
	}
	err = tmpl.Render(&buf, context)
	if err != nil {
		return err
	}

	return sendEmail(user.Username, user.Email, buf.Bytes())
}
