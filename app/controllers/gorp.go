package controllers

import (
	"fmt"
	"golang.org/x/crypto/bcrypt"
	"reflect"
	"strconv"
	"time"

	// "code.google.com/p/go.crypto/bcrypt"
	"database/sql"
	"github.com/go-gorp/gorp"
	// _ "github.com/mattn/go-sqlite3"
	"github.com/revel/modules/db/app"
	r "github.com/revel/revel"
	"necstpizza/app/models"
)

const (
	POPULATE_ON_START = false
)

var (
	Dbm *gorp.DbMap
)

func InitDB() {
	fmt.Printf("InitDB\n")
	db.Init()
	dbDriver := r.Config.StringDefault("db.driver", "sqlite3")
	if dbDriver == "sqlite3" {
		Dbm = &gorp.DbMap{Db: db.Db, Dialect: gorp.SqliteDialect{}}
	} else if dbDriver == "mymysql" {
		Dbm = &gorp.DbMap{Db: db.Db, Dialect: gorp.MySQLDialect{"InnoDB", "UTF8"}}
	}

	createTableFromModel(Dbm, models.Ingredient{})
	createTableFromModel(Dbm, models.Pizza{})
	createTableFromModel(Dbm, models.PizzaToIngredient{})
	createTableFromModel(Dbm, models.User{})
	createTableFromModel(Dbm, models.Event{})
	createTableFromModel(Dbm, models.PizzaOrder{})
	createTableFromModel(Dbm, models.Beverage{})
	createTableFromModel(Dbm, models.RecurrenceChild{})
	createTableFromModel(Dbm, models.PizzaOrderAddition{})
	createTableFromModel(Dbm, models.PizzaOrderRemoval{})

	Dbm.TraceOn("[gorp]", r.TRACE)

	Dbm.CreateTablesIfNotExists()

	if POPULATE_ON_START {
		Dbm.DropTables()
		Dbm.CreateTables()

		ingrs := []*models.Ingredient{
			&models.Ingredient{0, "Mozzarella", 1.0},
			&models.Ingredient{0, "Salame piccante", 1.0},
			&models.Ingredient{0, "Patatine fritte", 1.0},
		}

		for _, ingr := range ingrs {
			if err := Dbm.Insert(ingr); err != nil {
				panic(err)
			}
		}

		pizzas := []*models.Pizza{
			&models.Pizza{0, "Margherita", 0, 5.0, nil, []*models.Ingredient{ingrs[0]}},
			&models.Pizza{0, "Diavola", 0, 5.0, nil, []*models.Ingredient{ingrs[0], ingrs[1]}},
			&models.Pizza{0, "Patatine", 0, 5.0, nil, []*models.Ingredient{ingrs[0], ingrs[2]}},
		}
		for _, pizza := range pizzas {
			if err := Dbm.Insert(pizza); err != nil {
				panic(err)
			}
		}

		users := []*models.User{
			&models.User{"alessandro.frossi", models.ADMIN, "asd@asd.lol", false, "test", []byte{}, true},
		}
		for _, user := range users {
			user.HashedPassword, _ = bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
			if err := Dbm.Insert(user); err != nil {
				panic(err)
			}
		}

		events := []*models.Event{
			&models.Event{
				0,
				"Test Event",
				"alessandro.frossi",
				"No desc",
				"",
				"",
				-1,
				"alessandro.frossi",
				-1,
				"",
				true,
				false,
				"",
				false,
				time.Now().Add(2 * time.Hour),
				0,
				nil,
				time.Now().Add(1 * time.Hour),
				time.Now(),
			},
		}
		for _, event := range events {
			if err := Dbm.Insert(event); err != nil {
				panic(err)
			}
		}

		beverages := []*models.Beverage{
			&models.Beverage{0, "Coca cola"},
			&models.Beverage{0, "Coca zero"},
			&models.Beverage{0, "Fanta"},
			&models.Beverage{0, "Sprite"},
			&models.Beverage{0, "Acqua naturale"},
			&models.Beverage{0, "Acqua frizzante"},
		}

		for _, bev := range beverages {
			if err := Dbm.Insert(bev); err != nil {
				panic(err)
			}
		}
	}
}

func createTableFromModel(Dbm *gorp.DbMap, m interface{}) {
	// setColumnSizes := func(t *gorp.TableMap, colSizes map[string]int) {
	// 	for col, size := range colSizes {
	// 		t.ColMap(col).MaxSize = size
	// 	}
	// }

	// Keys
	mType := reflect.TypeOf(m)
	keys := make([]string, 0)
	autoInc := false
	for i := 0; i < mType.NumField(); i++ {
		// r.TRACE.Println("Autoinc for field", mType.Field(i).Name, ":", mType.Field(i).Tag.Get("db_autoincrement"))
		if mType.Field(i).Tag.Get("db_autoincrement") != "" {
			autoInc = true
		}
		if mType.Field(i).Tag.Get("db_key") != "" {
			keys = append(keys, mType.Field(i).Name)
		}
	}

	// Create table
	// r.TRACE.Printf("Creating table %s with autoincrement %s and keys %v\n", mType.Name(), strings.ToUpper(fmt.Sprintf("%s", autoInc)), keys)
	t := Dbm.AddTable(m).SetKeys(autoInc, keys...)
	for i := 0; i < mType.NumField(); i++ {
		// if alias := mType.Field(i).Tag.Get("db"); alias != "" {
		// 	fmt.Println("Remapping", mType.Field(i).Name, "to", alias)
		// 	t.ColMap(mType.Field(i).Name).Rename(alias)
		// }
		if mType.Field(i).Tag.Get("db_transient") != "" {
			t.ColMap(mType.Field(i).Name).Transient = true
		}
		if sz := mType.Field(i).Tag.Get("db_size"); sz != "" {
			szI, err := strconv.ParseInt(sz, 10, 64)
			if err != nil {
				panic(err)
			}
			t.ColMap(mType.Field(i).Name).MaxSize = int(szI)
		}
	}
}

type GorpController struct {
	*r.Controller
	Txn *gorp.Transaction
}

func (c *GorpController) Begin() r.Result {
	txn, err := Dbm.Begin()
	if err != nil {
		panic(err)
	}
	c.Txn = txn
	return nil
}

func (c *GorpController) Commit() r.Result {
	if c.Txn == nil {
		return nil
	}
	if err := c.Txn.Commit(); err != nil && err != sql.ErrTxDone {
		panic(err)
	}
	c.Txn = nil
	return nil
}

func (c *GorpController) Rollback() r.Result {
	if c.Txn == nil {
		return nil
	}
	if err := c.Txn.Rollback(); err != nil && err != sql.ErrTxDone {
		panic(err)
	}
	c.Txn = nil
	return nil
}
