package controllers

import (
	"fmt"
	"github.com/kirves/go-form-it"
	"github.com/kirves/go-form-it/fields"
	"github.com/revel/revel"
	"necstpizza/app/models"
	"necstpizza/app/routes"
	"sort"
)

type Orders struct {
	App
}

func (c Orders) checkUser() revel.Result {
	user := c.connected()
	if user == nil {
		return c.Redirect(routes.App.Index())
	}
	if user.Email == "" {
		c.Flash.Error("Before moving on please add your email address.")
		return c.Redirect(routes.UserController.EditUserView(user.Username))
	}
	return nil
}

func (c Orders) Participate(eventid int) revel.Result {
	u := c.connected()
	tmp, err := Dbm.Get(models.Event{}, eventid)
	if err != nil {
		panic(err)
	}

	if tmp == nil {
		return c.NotFound("")
	}

	event := tmp.(*models.Event)

	if !event.InTime() {
		return c.Forbidden("")
	}

	orders, err := Dbm.Select(models.PizzaOrder{}, "select * from PizzaOrder where EventId = ? and UserId = ?", eventid, u.Username)
	if err != nil {
		panic(err)
	}

	var orderTotal float32 = 0.0
	myOrders := make([]*forms.Form, len(orders))
	for i, ord := range orders {
		f := c.orderForm(revel.MainRouter.Reverse("Orders.DeleteOrder", nil).URL, c.orderItems())
		f.Field("beverage").SetValue(fmt.Sprintf("%d", ord.(*models.PizzaOrder).Drink.Id)).Disabled()
		f.Field("pizza").SetValue(fmt.Sprintf("%d", ord.(*models.PizzaOrder).Pizza.Id)).Disabled()
		f.Field("qty").SetValue(fmt.Sprintf("%d", ord.(*models.PizzaOrder).Qty)).Disabled()
		if len(ord.(*models.PizzaOrder).Additions) > 0 {
			f.Elements(
				fields.TextField("additions").SetValue(fmt.Sprintf("%s", ord.(*models.PizzaOrder).AdditionList())).AddClass("ingredient-addition").Disabled(),
			)
		} else {
			f.Elements(
				fields.TextField("additions").SetValue("No additions").Disabled(),
			)
		}
		if len(ord.(*models.PizzaOrder).Removals) > 0 {
			f.Elements(
				fields.TextField("removals").SetValue(fmt.Sprintf("%s", ord.(*models.PizzaOrder).RemovalList())).AddClass("ingredient-removal").Disabled(),
			)
		} else {
			f.Elements(
				fields.TextField("removals").SetValue("No removals").Disabled(),
			)
		}
		f.Elements(
			fields.HiddenField("eventid").SetValue(fmt.Sprintf("%d", eventid)),
			fields.HiddenField("orderid").SetValue(fmt.Sprintf("%d", ord.(*models.PizzaOrder).OrderId)),
		)
		if event.Open() {
			f.Elements(
				fields.SubmitButton("delete", "X").AddClass("btn-danger"),
			)
		}
		myOrders[i] = f
		orderTotal += ord.(*models.PizzaOrder).Price(event.SpecialPrice)
	}

	// NEW ORDER FORM
	form := c.orderForm(revel.MainRouter.Reverse("Orders.PlaceOrder", nil).URL, c.orderItems())
	form.SetParam("name", "neworderform")
	form.Field("pizza").SetId("chosenpizza")
	ingrOpts := map[string][]fields.InputChoice{}
	tmpIng, err := Dbm.Select(models.Ingredient{}, "select * from Ingredient")
	if err != nil {
		panic(err)
	}
	for _, b := range tmpIng {
		ing := b.(*models.Ingredient)
		prVal := fmt.Sprintf("+%.2f€", ing.Price)
		if _, ok := ingrOpts[prVal]; !ok {
			ingrOpts[prVal] = make([]fields.InputChoice, 0)
		}
		ingrOpts[prVal] = append(ingrOpts[prVal], fields.InputChoice{fmt.Sprintf("%d", ing.IngredientId), ing.Name})
	}
	form.Elements(
		fields.SelectField("addition", ingrOpts).MultipleChoice().AddClass("selectpicker").SetParam("data-live-search", "true").SetParam("title", "No additions").SetParam("data-selected-text-format", "count>3"),
	)
	form.Elements(
		fields.SelectField("removals", ingrOpts).MultipleChoice().AddClass("selectpicker").SetParam("data-live-search", "true").SetParam("title", "No removals").SetParam("data-selected-text-format", "count>3").SetId("removeingredients"),
	)
	form.Elements(
		fields.HiddenField("eventid").SetValue(fmt.Sprintf("%d", eventid)),
		fields.SubmitButton("submit", "Place").AddClass("btn-primary"),
	)

	// if manager, show already placed order, and allow ignore
	if event.ManagerId == u.Username || u.Admin {
		// placedOrders, err := c.Txn.Select(models.PizzaOrder{}, "select * from PizzaOrder where EventId = ? and UserId != ?", eventid, u.Username)
		placedOrders, err := Dbm.Select(models.PizzaOrder{}, "select * from PizzaOrder where EventId = ?", eventid)
		if err != nil {
			panic(err)
		}
		numElems := 0
		for _, ord := range placedOrders {
			numElems += ord.(*models.PizzaOrder).Qty
		}
		c.ViewArgs["orders"] = placedOrders
		c.ViewArgs["numElems"] = numElems
	}

	c.ViewArgs["event"] = event
	c.ViewArgs["form"] = form
	c.ViewArgs["oldForms"] = myOrders
	c.ViewArgs["total"] = orderTotal

	return c.Render()
}

func (c Orders) PlaceOrder(eventid int, pizza, beverage string, qty int, addition []string, removals []string) revel.Result {
	c.Validation.Min(qty, 1).Message("Invalid quantity.")

	tmp, err := Dbm.Get(models.Pizza{}, pizza)
	if err != nil {
		panic(err)
	}
	if tmp == nil {
		c.Validation.Error("Invalid entry.").Key("pizza")
	}
	pizzaObj := tmp.(*models.Pizza)

	tmp, err = Dbm.Get(models.Beverage{}, beverage)
	if err != nil {
		panic(err)
	}
	if tmp == nil {
		c.Validation.Error("Invalid entry.").Key("beverage")
	}
	bevObj := tmp.(*models.Beverage)

	if c.Validation.HasErrors() {
		c.Validation.Keep()
		return c.Redirect(routes.Orders.Participate(eventid))
	}

	tmp, err = Dbm.Get(models.Event{}, eventid)
	if err != nil {
		panic(err)
	}
	if tmp == nil {
		return c.NotFound("")
	}
	eventObj := tmp.(*models.Event)

	if !eventObj.Open() {
		return c.Forbidden("")
	}

	adds := make([]*models.Ingredient, 0)
	for _, v := range addition {
		tmp, err := Dbm.Get(models.Ingredient{}, v)
		if err != nil {
			panic(err)
		}
		if tmp != nil {
			adds = append(adds, tmp.(*models.Ingredient))
		}
	}

	rems := make([]*models.Ingredient, 0)
	for _, v := range removals {
		tmp, err := Dbm.Get(models.Ingredient{}, v)
		if err != nil {
			panic(err)
		}
		if tmp != nil {
			rems = append(rems, tmp.(*models.Ingredient))
		}
	}

	order := &models.PizzaOrder{
		0,
		0,
		"",
		0,
		0,
		qty,
		false,
		false,
		eventObj,
		c.connected(),
		pizzaObj,
		bevObj,
		adds,
		rems,
	}

	err = c.Txn.Insert(order)
	if err != nil {
		panic(err)
	}

	// c.Flash.Success("Successfully placed order.")
	return c.Redirect(routes.Orders.Participate(eventid))
}

func (c Orders) DeleteOrder(eventid int, orderid int, pizza, beverage string, qty int) revel.Result {
	tmp, err := c.Txn.Get(models.PizzaOrder{}, orderid)
	if err != nil {
		panic(err)
	}
	if tmp == nil {
		return c.NotFound("order")
	}

	order := tmp.(*models.PizzaOrder)

	tmp, err = Dbm.Get(models.Event{}, eventid)
	if err != nil {
		panic(err)
	}
	if tmp == nil {
		return c.NotFound("event")
	}
	eventObj := tmp.(*models.Event)

	if !eventObj.Open() {
		return c.NotFound("")
	}

	if order.User.Username != c.connected().Username || order.Event.EventId != eventObj.EventId {
		return c.Forbidden("")
	}

	_, err = c.Txn.Delete(order)
	if err != nil {
		panic(err)
	}

	return c.Redirect(routes.Orders.Participate(eventid))
}

func (c Orders) orderItems() []map[string][]fields.InputChoice {
	tmp, err := Dbm.Select(models.Pizza{}, "select * from Pizza")
	if err != nil {
		panic(err)
	}
	sort.Sort(models.NewPizzaSorter(tmp))

	pizzaOpts := map[string][]fields.InputChoice{
		"": make([]fields.InputChoice, 0),
	}
	for _, p := range tmp {
		pizzaOpts[""] = append(pizzaOpts[""], fields.InputChoice{fmt.Sprintf("%d", p.(*models.Pizza).Id), p.(*models.Pizza).Name})
	}

	tmp, err = Dbm.Select(models.Beverage{}, "select * from Beverage")
	if err != nil {
		panic(err)
	}

	bevOpts := map[string][]fields.InputChoice{
		"": make([]fields.InputChoice, 0),
	}
	for _, b := range tmp {
		bevOpts[""] = append(bevOpts[""], fields.InputChoice{fmt.Sprintf("%d", b.(*models.Beverage).Id), b.(*models.Beverage).Name})
	}

	qtyOpts := map[string][]fields.InputChoice{
		"": make([]fields.InputChoice, 0),
	}
	for i := 1; i <= 5; i++ {
		val := fmt.Sprintf("%d", i)
		qtyOpts[""] = append(qtyOpts[""], fields.InputChoice{val, val})
	}

	return []map[string][]fields.InputChoice{pizzaOpts, bevOpts, qtyOpts}
}

func (c Orders) orderForm(action string, opts []map[string][]fields.InputChoice) *forms.Form {
	pizzaOpts := opts[0]
	bevOpts := opts[1]
	qtyOpts := opts[2]
	form := forms.BootstrapForm(forms.POST, action).Elements(
		fields.SelectField("pizza", pizzaOpts).AddClass("selectpicker").AddClass("show-tick").SetParam("data-live-search", "true").SetValue("1"),
		fields.SelectField("beverage", bevOpts).AddClass("selectpicker").AddClass("show-tick"),
		fields.SelectField("qty", qtyOpts).AddClass("selectpicker").AddClass("show-tick"),
	).AddClass("form-inline")

	if c.Validation.HasErrors() {
		for k, err := range c.Validation.ErrorMap() {
			form.Field(k).AddError(err.Message)
		}
	}

	return form
}

func (c Orders) IgnoreOrder(id int) revel.Result {
	u := c.connected()
	tmp, err := Dbm.Get(models.PizzaOrder{}, id)
	if err != nil {
		panic(err)
	}

	if tmp == nil {
		return c.NotFound("")
	}
	order := tmp.(*models.PizzaOrder)
	if order.Event.ManagerId != u.Username {
		return c.Forbidden("")
	}

	order.Ignore = true

	_, err = c.Txn.Update(order)
	if err != nil {
		panic(err)
	}

	return c.Redirect(routes.Orders.Participate(order.Event.EventId))
}

func (c Orders) RestoreOrder(id int) revel.Result {
	u := c.connected()
	tmp, err := Dbm.Get(models.PizzaOrder{}, id)
	if err != nil {
		panic(err)
	}

	if tmp == nil {
		return c.NotFound("")
	}
	order := tmp.(*models.PizzaOrder)
	if order.Event.ManagerId != u.Username {
		return c.Forbidden("")
	}

	order.Ignore = false

	_, err = c.Txn.Update(order)
	if err != nil {
		panic(err)
	}

	return c.Redirect(routes.Orders.Participate(order.Event.EventId))
}

func (c Orders) PayOrder(id int) revel.Result {
	u := c.connected()
	tmp, err := Dbm.Get(models.PizzaOrder{}, id)
	if err != nil {
		panic(err)
	}

	if tmp == nil {
		return c.NotFound("")
	}
	order := tmp.(*models.PizzaOrder)
	if order.Event.ManagerId != u.Username {
		return c.Forbidden("")
	}

	order.Paid = true

	_, err = c.Txn.Update(order)
	if err != nil {
		panic(err)
	}

	return c.Redirect(routes.Orders.Participate(order.Event.EventId))
}

func (c Orders) UnpayOrder(id int) revel.Result {
	u := c.connected()
	tmp, err := Dbm.Get(models.PizzaOrder{}, id)
	if err != nil {
		panic(err)
	}

	if tmp == nil {
		return c.NotFound("")
	}
	order := tmp.(*models.PizzaOrder)
	if order.Event.ManagerId != u.Username {
		return c.Forbidden("")
	}

	order.Paid = false

	_, err = c.Txn.Update(order)
	if err != nil {
		panic(err)
	}

	return c.Redirect(routes.Orders.Participate(order.Event.EventId))
}
