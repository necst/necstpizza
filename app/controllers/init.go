package controllers

import (
	"necstpizza/app/models"
	"reflect"
	"strings"
	"time"
	// "bitbucket.org/kirves/gopizza/app/models"
	"fmt"
	"github.com/revel/revel"
)

func init() {
	revel.OnAppStart(InitDB)

	revel.InterceptMethod((*GorpController).Begin, revel.BEFORE)
	revel.InterceptMethod((*GorpController).Commit, revel.AFTER)
	revel.InterceptMethod((*GorpController).Rollback, revel.PANIC)

	revel.InterceptMethod(App.AddUser, revel.BEFORE)
	revel.InterceptMethod(App.AddEvents, revel.BEFORE)
	revel.InterceptMethod(Elements.checkUser, revel.BEFORE)
	revel.InterceptMethod(Events.checkUser, revel.BEFORE)
	revel.InterceptMethod(UserController.checkUser, revel.BEFORE)
	revel.InterceptMethod(Orders.checkUser, revel.BEFORE)

	// AdminRegisterModel(models.Ingredient{})
	// AdminRegisterModel(models.Pizza{})

	revel.TemplateFuncs["euro"] = func(a float32) string { return fmt.Sprintf("%.2f €", a) }
	revel.TemplateFuncs["ingredients"] = func(p models.Pizza) string {
		return strings.Join(p.IngredientsNames(), ", ")
	}
	revel.TemplateFuncs["slice"] = func(a interface{}, b int) [][]interface{} {
		if a == nil {
			return nil
		}
		val := reflect.ValueOf(a)

		origLen := val.Len()
		outerLen := origLen / b
		if origLen%b > 0 {
			outerLen++
		}

		// Make the output slices with all the correct lengths
		c := make([][]interface{}, outerLen)

		itemsLeft := origLen
		for i := range c {
			if itemsLeft < b {
				c[i] = make([]interface{}, itemsLeft)
			} else {
				c[i] = make([]interface{}, b)
			}
			itemsLeft -= b
		}

		// now populate our slices via reflection
		for i, s := range c {
			subSlice := val.Slice(i*b, i*b+len(s))
			for j := 0; j < len(s); j++ {
				c[i][j] = subSlice.Index(j).Interface()
			}
		}

		return c
	}

	revel.TemplateFuncs["days"] = func(dt time.Duration) int {
		return int(dt.Hours() / 24)
	}

	revel.TemplateFuncs["niceDate"] = func(t time.Time) string {
		return t.Format("Mon Jan 2 2006 - 15:04")
	}

	revel.TemplateFuncs["fullDate"] = func(t time.Time) string {
		return t.Format("Monday Jan 2, 2006")
	}

	revel.TemplateFuncs["hour"] = func(t time.Time) string {
		return t.Format("15:04")
	}

	revel.TemplateFuncs["day"] = func(t time.Time) string {
		return t.Format("Monday")
	}

	revel.TemplateFuncs["addList"] = func(ord *models.PizzaOrder) string {
		return ord.AdditionList()
	}

	revel.TemplateFuncs["remList"] = func(ord *models.PizzaOrder) string {
		return ord.RemovalList()
	}

	revel.TemplateFuncs["userEnabled"] = func(user *models.User, manager string) bool {
		return user.Admin || user.Username == manager
	}

}
