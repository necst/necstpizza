package controllers

import (
	"fmt"
	"github.com/kirves/go-form-it"
	"github.com/kirves/go-form-it/fields"
	"github.com/revel/revel"
	"necstpizza/app/models"
	"necstpizza/app/routes"
	"sort"
	"strconv"
	"time"
)

type Events struct {
	App
}

func (c Events) checkUser() revel.Result {
	user := c.connected()
	if user == nil {
		return c.Redirect(routes.App.Index())
	}
	if user.Email == "" {
		c.Flash.Error("Before moving on please add your email address.")
		return c.Redirect(routes.UserController.EditUserView(user.Username))
	}
	return nil
}

func (c Events) CalendarView() revel.Result {
	return c.Render()
}

func (c Events) EventsToJson() revel.Result {
	type OutEvent struct {
		Title  string `json:"title"`
		Start  string `json:"start"`
		AllDay bool   `json:"allDay"`
		Url    string `json:"url"`
	}

	tmp, _ := c.ViewArgs["events"]
	evrng := tmp
	events := make([]OutEvent, 0)
	for _, tmp2 := range evrng.([]interface{}) {
		ev := tmp2.(*models.Event)
		events = append(events, OutEvent{
			ev.Name,
			ev.Timestamp.Format(models.SQL_DATE_FORMAT),
			false,
			routes.Orders.Participate(ev.EventId),
		})
	}

	return c.RenderJSON(events)
}

func (c Events) ListEvents() revel.Result {
	u := c.connected()
	if !u.Admin {
		return c.Forbidden("")
	}
	return c.Render()
}

func (c Events) ListOldEvents() revel.Result {
	u := c.connected()
	if !u.Admin {
		return c.Forbidden("")
	}

	var query string
	dbDriver := revel.Config.StringDefault("db.driver", "sqlite3")
	if dbDriver == "sqlite3" {
		query = "select * from Event where datetime('now', 'localtime') > TimestampString"
	} else if dbDriver == "mymysql" {
		query = "select * from Event where NOW() > TimestampString"
	}
	events, err := Dbm.Select(models.Event{}, query)
	if err != nil {
		panic(err)
	}
	sort.Reverse(models.NewEventSorter(events))

	c.ViewArgs["old_events"] = events

	return c.Render()
}

func (c Events) ListRecurrentEvents() revel.Result {
	u := c.connected()
	if !u.Admin {
		return c.Forbidden("")
	}

	events, err := Dbm.Select(models.Event{}, "select * from Event where RecurrenceInt > 0")
	if err != nil {
		panic(err)
	}
	sort.Reverse(models.NewEventSorter(events))

	c.ViewArgs["rec_events"] = events

	return c.Render()
}

func (c Events) CreateEventView() revel.Result {
	u := c.connected()
	if !u.Admin {
		return c.Forbidden("")
	}

	form := c.createEventForm(revel.MainRouter.Reverse("Events.CreateEvent", nil).URL)
	form.Elements(
		fields.SubmitButton("submit", "Create").AddClass("force-left"),
	)

	form.Field("name").SetValue(c.Flash.Data["name"])
	form.Field("description").SetText(c.Flash.Data["description"])
	form.Field("addInfo").SetText(c.Flash.Data["addInfo"])
	form.Field("when").SetValue(c.Flash.Data["when"])
	form.Field("notification").SetValue(c.Flash.Data["notification"])
	form.Field("repeat").SetValue(c.Flash.Data["repeat"])
	form.Field("manager").SetValue(c.Flash.Data["manager"])
	if c.Flash.Data["special"] != "" {
		form.Field("special").AddTag("checked")
	}

	c.ViewArgs["form"] = form
	return c.Render()
}

func (c Events) EditEventView(id int) revel.Result {
	u := c.connected()
	if !u.Admin {
		return c.Forbidden("")
	}

	tmp, err := Dbm.Get(models.Event{}, id)
	if err != nil {
		panic(err)
	}
	if tmp == nil {
		return c.NotFound("")
	}

	event := tmp.(*models.Event)
	form := c.createEventForm(revel.MainRouter.Reverse("Events.EditEvent", nil).URL)
	form.Elements(
		fields.HiddenField("id").SetValue(fmt.Sprintf("%d", id)),
	)

	if event.InTime() {
		form.Elements(
			fields.SubmitButton("submit", "Save").AddClass("force-left"),
			fields.Button("delete", "Delete").AddClass("btn-danger").SetParam("onClick", fmt.Sprintf("location.href='%s'", routes.Events.DeleteEvent(id))),
		)
	}

	if c.Flash.Data["name"] == "" {
		form.Field("name").SetValue(event.Name)
	} else {
		form.Field("name").SetValue(c.Flash.Data["name"])
	}
	if c.Flash.Data["description"] == "" {
		form.Field("description").SetText(event.Description)
	} else {
		form.Field("description").SetText(c.Flash.Data["description"])
	}
	if c.Flash.Data["addInfo"] == "" {
		form.Field("addInfo").SetText(event.AdditionalInfo)
	} else {
		form.Field("addInfo").SetText(c.Flash.Data["addInfo"])
	}
	if c.Flash.Data["when"] == "" {
		form.Field("when").SetValue(event.TimeStampString)
	} else {
		form.Field("when").SetValue(c.Flash.Data["when"])
	}
	if c.Flash.Data["notification"] == "" {
		form.Field("notification").SetValue(event.NotificationTimeString)
	} else {
		form.Field("notification").SetValue(c.Flash.Data["notification"])
	}
	if c.Flash.Data["repeat"] == "" {
		form.Field("repeat").SetValue(fmt.Sprintf("%d", event.RecurrenceInt/24))
	} else {
		form.Field("repeat").SetValue(c.Flash.Data["repeat"])
	}

	if _, ok := c.Flash.Data["manager"]; !ok {
		if event.ManagerId != "" {
			form.Field("manager").SetValue(event.ManagerId)
		} else {
			form.Field("manager").SetValue("")
		}
	} else {
		form.Field("manager").SetValue(c.Flash.Data["manager"])
	}

	if _, ok := c.Flash.Data["special"]; ok || event.SpecialPrice {
		form.Field("special").AddTag("checked")
	}

	if !event.InTime() {
		form.Field("name").Disabled()
		form.Field("description").Disabled()
		form.Field("when").Disabled()
		form.Field("notification").Disabled()
		form.Field("manager").Disabled()
		if !event.Recurrent() {
			form.Field("repeat").Disabled()
		} else {
			form.Elements(
				fields.SubmitButton("submit", "Save").AddClass("force-left"),
			)
		}
	}

	c.ViewArgs["form"] = form
	return c.Render()
}

func (c Events) EditEvent(id int, name, description, addInfo, when, notification, repeat, manager, special, notify string) revel.Result {
	u := c.connected()
	if !u.Admin {
		return c.Forbidden("")
	}

	tmp, err := Dbm.Get(models.Event{}, id)
	if err != nil {
		panic(err)
	}
	if tmp == nil {
		return c.NotFound("")
	}
	mustNotify := false
	event := tmp.(*models.Event)

	var mgrOut *models.User = nil

	if event.InTime() {

		evTs, noTs, rec, mgr := c.validateEvent(name, description, when, notification, repeat, manager)
		mgrOut = mgr

		if c.Validation.HasErrors() {
			c.Validation.Keep()
			c.FlashParams()
			return c.Redirect(routes.Events.EditEventView(id))
		}

		event.Name = name
		event.Description = description
		event.AdditionalInfo = addInfo
		event.Timestamp = evTs
		event.NotificationTime = noTs
		event.Recurrence = time.Duration(time.Duration(rec) * 24 * time.Hour)
		if special == "" {
			event.SpecialPrice = false
		} else {
			event.SpecialPrice = true
		}

		if mgr != nil {
			event.Active = true
			if mgr != nil && mgr.Username != event.ManagerId {
				mustNotify = true
			}
			event.ManagerId = mgr.Username
		} else {
			event.Active = false
			event.ManagerId = ""
		}
	} else {
		if event.Recurrent() {
			rec := c.validateRepeat(repeat)
			event.Recurrence = time.Duration(time.Duration(rec) * 24 * time.Hour)
		} else {
			return c.Forbidden("")
		}
	}

	_, err = c.Txn.Update(event)
	if err != nil {
		panic(err)
	}

	if mustNotify && mgrOut != nil {
		// NOTIFY
		go func() {
			err := notifyAssignee(mgrOut.Username, mgrOut.Email, id)
			if err != nil {
				panic(err)
			}
		}()
	}

	if notify != "" {
		go func() {
			err := notifyMailingList(mgrOut, event)
			if err != nil {
				panic(err)
			}
		}()
	}

	c.Flash.Success("Successfully edited event: %s", event.Name)
	return c.Redirect(routes.Events.ListEvents())
}

func (c Events) CreateEvent(name, description, addInfo, when, notification, repeat, manager, special, notify string) revel.Result {
	// fmt.Println("SPECIAL:", special)
	u := c.connected()
	if !u.Admin {
		return c.Forbidden("")
	}

	evTs, noTs, rec, mgr := c.validateEvent(name, description, when, notification, repeat, manager)

	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(routes.Events.CreateEventView())
	}

	event := &models.Event{
		0,
		name,
		"",
		description,
		addInfo,
		"",
		-1,
		u.Username,
		-1,
		"",
		false,
		false,
		"",
		false,
		evTs,
		time.Duration(time.Duration(rec) * 24 * time.Hour),
		nil,
		noTs,
		time.Now(),
	}

	if mgr != nil {
		event.Active = true
		event.ManagerId = mgr.Username
	}

	if special == "" {
		event.SpecialPrice = false
	} else {
		event.SpecialPrice = true
	}

	err := c.Txn.Insert(event)
	if err != nil {
		panic(err)
	}

	if mgr != nil {
		// NOTIFY
		go func() {
			err := notifyAssignee(mgr.Username, mgr.Email, event.EventId)
			if err != nil {
				panic(err)
			}
		}()
	}

	if notify != "" {
		go func() {
			err := notifyMailingList(mgr, event)
			if err != nil {
				panic(err)
			}
		}()
	}

	c.Flash.Success("Successfully created event: %s", event.Name)
	return c.Redirect(routes.Events.ListEvents())
}

func (c Events) DeleteEvent(id int) revel.Result {
	u := c.connected()
	if !u.Admin {
		return c.Forbidden("")
	}

	tmp, err := c.Txn.Get(models.Event{}, id)
	if err != nil {
		panic(err)
	}
	if tmp == nil {
		return c.NotFound("")
	}

	event := tmp.(*models.Event)

	if !event.InTime() {
		return c.Forbidden("")
	}

	_, err = c.Txn.Delete(event)
	if err != nil {
		panic(err)
	}

	c.Flash.Success("Successfully deleted event: %s", event.Name)
	return c.Redirect(routes.Events.ListEvents())
}

func (c Events) createEventForm(action string) *forms.Form {
	userOpts := map[string][]fields.InputChoice{
		"Inactive": []fields.InputChoice{
			fields.InputChoice{"", "-- Inactive event --"},
		},
		"":       []fields.InputChoice{},
		"Admins": []fields.InputChoice{},
	}
	users, err := Dbm.Select(models.User{}, "select * from User")
	if err != nil {
		panic(err)
	}
	for _, tmp := range users {
		user := tmp.(*models.User)
		if user.Enabled {
			if user.Admin {
				userOpts["Admins"] = append(userOpts["Admins"], fields.InputChoice{user.Username, user.String()})
			} else {
				userOpts[""] = append(userOpts[""], fields.InputChoice{user.Username, user.String()})
			}
		}
	}
	notificationOpts := map[string][]fields.InputChoice{
		"": []fields.InputChoice{
			fields.InputChoice{"0", "-- No recurrence --"},
		},
	}
	for i := 1; i <= 14; i++ {
		notificationOpts[""] = append(notificationOpts[""], fields.InputChoice{
			fmt.Sprintf("%d", i), fmt.Sprintf("Repeat every %d days", i)})
	}

	form := forms.BootstrapForm(forms.POST, action).Elements(
		fields.TextField("name").SetLabel("Event name"),
		fields.TextAreaField("description", 50, 20).SetLabel("Description"),
		fields.TextAreaField("addInfo", 50, 50).SetLabel("Additional info").SetParam("placeholder", "[Insert info about pre-pizza meetings or talks here]").SetParam("rows", "10"),
		forms.FieldSet("timestamps",
			fields.DatetimeField("when").SetLabel("When").SetId("datetimepicker1"),
			fields.DatetimeField("notification").SetLabel("Notification").SetId("datetimepicker2"),
		).AddClass("left"),
		forms.FieldSet("details",
			fields.SelectField("repeat", notificationOpts).SetLabel("Repeat every (days)"),
			fields.SelectField("manager", userOpts).SetLabel("Person to be notified").SetValue(""),
		).AddClass("right"),
		fields.Checkbox("special", false).SetLabel("This is a 'special price' event.").AddClass("force-left"),
		fields.Checkbox("notify", false).SetLabel("Send e-mail notification for this event.").AddClass("force-left"),
	)

	if c.Validation.HasErrors() {
		for k, err := range c.Validation.ErrorMap() {
			form.Field(k).AddError(err.Message)
		}
	}

	return form
}

func (c Events) validateEvent(name, description, when, notification, repeat, manager string) (time.Time, time.Time, int, *models.User) {
	c.Validation.Required(name).Message("Name is required.")

	c.Validation.Required(when).Message("Missing event timestamp.")

	dt1, err := time.ParseInLocation(models.SQL_DATE_FORMAT, when, time.Local)
	if err != nil {
		c.Validation.Error("Invalid date.").Key("when")
	} else {
		if dt1.Before(time.Now()) {
			c.Validation.Error("Event can't be in the past.").Key("when")
		}
	}

	dt2, err := time.ParseInLocation(models.SQL_DATE_FORMAT, notification, time.Local)
	if err != nil {
		c.Validation.Error("Invalid date.").Key("notification")
	} else {
		if dt2.Before(time.Now()) {
			c.Validation.Error("Notification time can't be in the past.").Key("notification")
		}
		if !dt1.IsZero() && dt1.Before(dt2) {
			c.Validation.Error("Notification time must be before event time.").Key("notification")
		}
	}

	rptVal := c.validateRepeat(repeat)

	var user *models.User = nil
	if manager != "" {
		tmp, err := Dbm.Get(models.User{}, manager)
		if err != nil {
			panic(err)
		}
		if tmp == nil {
			c.Validation.Error("Invalid user.").Key("manager")
		}
		user = tmp.(*models.User)
	}

	return dt1, dt2, rptVal, user
}

func (c Events) validateRepeat(repeat string) int {
	c.Validation.Required(repeat).Message("Missing repeat value.")
	rptVal, err := strconv.ParseInt(repeat, 10, 0)
	if err != nil {
		c.Validation.Error("Invalid repeat time.").Key("repeat")
	} else {
		if rptVal < 0 || rptVal > 14 {
			c.Validation.Error("Invalid repeat time.").Key("repeat")
		}
	}
	return int(rptVal)
}
