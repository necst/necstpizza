package controllers

import (
	"fmt"
	"github.com/revel/revel"
	"reflect"
	"strings"
)

var (
	adminRegModels map[string]AdminCompatible // = make(map[string]interface{})
)

type AdminCompatible interface {
	GetAll(txn ...interface{}) []interface{}
}

func AdminRegisterModel(m AdminCompatible) {
	if adminRegModels == nil {
		adminRegModels = make(map[string]AdminCompatible)
	}
	adminRegModels[strings.ToLower(reflect.TypeOf(m).Name())] = m
}

type Admin struct {
	GorpController
}

func (c Admin) Index() revel.Result {
	names := make([]string, len(adminRegModels))
	i := 0
	for _, v := range adminRegModels {
		names[i] = reflect.TypeOf(v).Name()
		i++
	}
	return c.Render(names)
}

func (c Admin) ListElements(name string) revel.Result {
	m, ok := adminRegModels[name]
	if !ok {
		return c.NotFound("Model not found.")
	}
	elems := m.GetAll(c.Txn)
	return c.Render(elems)
}

func (c Admin) AddElement(name string) revel.Result {
	m, ok := adminRegModels[name]
	if !ok {
		return c.NotFound("Model not found.")
	}
	t := reflect.TypeOf(m)
	fields := make([]string, t.NumField())
	for i := 0; i < t.NumField(); i++ {
		fields[i] = fmt.Sprintf("%s (%s)", t.Field(i).Name, t.Field(i).Tag.Get("admin"))
	}
	return c.Render(fields)
}
