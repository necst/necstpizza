package controllers

import (
	"github.com/kirves/go-form-it"
	"github.com/kirves/go-form-it/fields"
	"golang.org/x/crypto/bcrypt"
	// "github.com/kirves/goradius"
	"github.com/revel/revel"
	"necstpizza/app/models"
	"necstpizza/app/routes"
	"sort"
)

type App struct {
	GorpController
}

func (c App) AddUser() revel.Result {
	if user := c.connected(); user != nil {
		c.ViewArgs["user"] = user
	}
	return nil
}

func (c App) AddEvents() revel.Result {
	var query string
	dbDriver := revel.Config.StringDefault("db.driver", "sqlite3")
	if dbDriver == "sqlite3" {
		query = "select * from Event where datetime('now', 'localtime') < TimestampString"
	} else if dbDriver == "mymysql" {
		query = "select * from Event where NOW() < TimestampString"
	}
	events, err := Dbm.Select(models.Event{}, query)
	if err != nil {
		panic(err)
	}
	sort.Sort(models.NewEventSorter(events))

	c.ViewArgs["events"] = events
	return nil
}

func (c App) Index() revel.Result {
	if c.ViewArgs["form"] == nil {
		actionUrl := revel.MainRouter.Reverse("App.Login", nil).URL
		form := forms.BootstrapForm(forms.POST, actionUrl).Elements(
			fields.TextField("username").SetLabel("Username").SetValue(c.Flash.Data["username"]),
			fields.PasswordField("password").SetLabel("Password"),
			fields.SubmitButton("submit", "Submit"),
		).AddClass("login-form")

		if c.Validation.HasErrors() {
			for k, err := range c.Validation.ErrorMap() {
				form.Field(k).AddError(err.Message)
			}
		}
		c.ViewArgs["form"] = form
	}
	return c.Render()
}

func (c App) Login(username, password string) revel.Result {
	c.Validation.Required(username).Message("%s", "Username is required.")
	c.Validation.Required(password).Message("%s", "Password is required.")

	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(routes.App.Index())
	}

	user := c.getUser(username)
	if user != nil && user.Enabled {
		err := bcrypt.CompareHashAndPassword(user.HashedPassword, []byte(password))
		if err == nil {
			c.Session["user"] = username
			c.Flash.Success("Welcome back, %s", user)
		} else {
			c.FlashParams()
			c.Flash.Error("Wrong username / password.")
			return c.Redirect(routes.App.Index())
		}
	} else {
		// if user == nil {
		// 	auth := goradius.Authenticator(revel.Config.StringDefault("radius.server", "127.0.0.1"),
		// 		revel.Config.StringDefault("radius.port", "1812"),
		// 		revel.Config.StringDefault("radius.secret", ""))
		// 	ok, err := auth.Authenticate(username, password)
		// 	if ok && err == nil {
		// 		// create user - redirect to edit user
		// 		psw, _ := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
		// 		user = &models.User{username, models.USER, "", false, "", psw, true}
		// 		c.Txn.Insert(user)
		// 		c.Session["user"] = username
		// 		c.Flash.Success("User created, please set your password and save.")
		// 		return c.Redirect(routes.UserController.EditUserView(username))
		// 	} else {
		// 		if err != nil {
		// 			panic(err)
		// 		}
		// 	}
		// }
		c.FlashParams()
		c.Flash.Error("Wrong username / password.")
		return c.Redirect(routes.App.Index())
	}
	return c.Redirect(routes.Events.CalendarView())
}

func (c App) Logout() revel.Result {
	delete(c.Session, "user")
	delete(c.ViewArgs, "user")
	c.Flash.Success("Successfully logged out.")
	return c.Redirect(routes.App.Index())
}

func (c App) connected() *models.User {
	if c.ViewArgs["user"] != nil {
		return c.ViewArgs["user"].(*models.User)
	}
	if username, ok := c.Session["user"]; ok {
		return c.getUser(username)
	}
	return nil
}

func (c App) getUser(username string) *models.User {
	user, err := Dbm.Get(models.User{}, username)
	if err != nil {
		panic(err)
	}
	if user == nil {
		return nil
	}
	return user.(*models.User)
}
