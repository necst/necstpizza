
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE Event ADD COLUMN AdditionalInfo varchar(8192) default "";

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
CREATE TEMPORARY TABLE Event_backup("EventId" integer not null primary key autoincrement, "Name" varchar(30), "ManagerId" varchar(255), "Description" varchar(1024), "TimeStampString" varchar(30), "RecurrenceInt" integer, "CreatorId" varchar(100), "ParentId" integer, "NotificationTimeString" varchar(255), "Active" integer, Notified int DEFAULT 0, "CreateChildSince" varchar(255) DEFAULT "2014-03-20 15:00", SpecialPrice int DEFAULT 0);
INSERT INTO Event_backup SELECT "EventId", "Name", "ManagerId", "Description", "TimeStampString", "RecurrenceInt", "CreatorId", "ParentId", "NotificationTimeString", "Active", Notified, "CreateChildSince", SpecialPrice FROM Event;
DROP TABLE Event;
CREATE TABLE "Event" ("EventId" integer not null primary key autoincrement, "Name" varchar(30), "ManagerId" varchar(255), "Description" varchar(1024), "TimeStampString" varchar(30), "RecurrenceInt" integer, "CreatorId" varchar(100), "ParentId" integer, "NotificationTimeString" varchar(255), "Active" integer, Notified int DEFAULT 0, "CreateChildSince" varchar(255) DEFAULT "2014-03-20 15:00", SpecialPrice int DEFAULT 0);
INSERT INTO Event SELECT "EventId", "Name", "ManagerId", "Description", "TimeStampString", "RecurrenceInt", "CreatorId", "ParentId", "NotificationTimeString", "Active", Notified, "CreateChildSince", SpecialPrice FROM Event_backup;
DROP TABLE Event_backup;
