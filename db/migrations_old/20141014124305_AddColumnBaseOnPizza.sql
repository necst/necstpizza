
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE Pizza ADD COLUMN BaseId integer default 0;

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
CREATE TEMPORARY TABLE "Pizza_backup" ("Id" integer not null primary key autoincrement, "Name" varchar(30), "Price" real);
INSERT INTO Pizza_backup SELECT "Id", "Name", "Price" FROM Pizza;
DROP TABLE Pizza;
CREATE TABLE "Pizza" ("Id" integer not null primary key autoincrement, "Name" varchar(30), "Price" real);
INSERT INTO Pizza SELECT "Id", "Name", "Price" FROM Pizza_backup;
DROP TABLE Pizza_backup;