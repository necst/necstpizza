
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE "PizzaOrderRemoval" ("OrderId" integer not null, "IngredientId" integer not null, primary key ("OrderId", "IngredientId"));

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE "PizzaOrderRemoval";