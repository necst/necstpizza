
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TEMPORARY TABLE "Ingredient_bk" ("IngredientId" integer not null primary key autoincrement, "Name" varchar(30));
INSERT INTO "Ingredient_bk" SELECT "IngredientId", "Name" FROM Ingredient;
DROP TABLE Ingredient;
CREATE TABLE "Ingredient" ("IngredientId" integer not null primary key autoincrement, "Name" varchar(30));
INSERT INTO "Ingredient" SELECT "IngredientId", "Name" FROM Ingredient_bk;
DROP TABLE Ingredient_bk;

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE Ingredient ADD COLUMN Free int DEFAULT 0;
