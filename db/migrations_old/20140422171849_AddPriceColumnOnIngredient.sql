
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE Ingredient ADD COLUMN Price real DEFAULT 1;

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
CREATE TEMPORARY TABLE "Ingredient_bk" ("IngredientId" integer not null primary key autoincrement, "Name" varchar(30));
INSERT INTO "Ingredient_bk" SELECT "IngredientId", "Name" FROM Ingredient;
DROP TABLE Ingredient;
CREATE TABLE "Ingredient" ("IngredientId" integer not null primary key autoincrement, "Name" varchar(30));
INSERT INTO "Ingredient" SELECT "IngredientId", "Name" FROM Ingredient_bk;
DROP TABLE Ingredient_bk;
