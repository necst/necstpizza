
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE "RecurrenceChild" ("ParentId" integer not null, "ChildId" integer not null, primary key ("ParentId", "ChildId"));

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE "RecurrenceChild";

