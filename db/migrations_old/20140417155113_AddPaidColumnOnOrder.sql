
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE PizzaOrder ADD COLUMN Paid int DEFAULT 0;

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
CREATE TEMPORARY TABLE "PizzaOrder_bk" ("OrderId" integer not null primary key autoincrement, "EventId" integer, "UserId" varchar(20), "PizzaId" integer, "BeverageId" integer, "Qty" integer, Ignore int DEFAULT 0);
INSERT INTO "PizzaOrder_bk" SELECT "OrderId", "EventId", "UserId", "PizzaId", "BeverageId", "Qty", Ignore FROM PizzaOrder;
DROP TABLE PizzaOrder;
CREATE TABLE "PizzaOrder" ("OrderId" integer not null primary key autoincrement, "EventId" integer, "UserId" varchar(20), "PizzaId" integer, "BeverageId" integer, "Qty" integer, Ignore int DEFAULT 0);
INSERT INTO "PizzaOrder" SELECT "OrderId", "EventId", "UserId", "PizzaId", "BeverageId", "Qty", Ignore FROM PizzaOrder_bk;
DROP TABLE PizzaOrder_bk;
