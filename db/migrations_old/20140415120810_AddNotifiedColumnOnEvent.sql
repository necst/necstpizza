
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied
ALTER TABLE Event ADD COLUMN Notified int DEFAULT 0;

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE Event DROP COLUMN Notified;

